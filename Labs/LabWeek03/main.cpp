/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 15, 2015, 11:25 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    string var1 = "1";
    int var2 = 2;
    cout << var1 << " + " << var1 << " = " << var1 + var1 << endl;
    cout << var2 << " + " << var2 << " = " << var2 + var2 << endl << endl;
    
    //NUMBER 2
    
    double sluggingPercentage;
    int singles, doubles, triples, homeRuns, atBats;
    
    cout << "Enter the number of singles hit: ";
    cin >> singles;
    cout << "Enter the number of doubles hit: ";
    cin >> doubles;
    cout << "Enter the number of triples hit: ";
    cin >> triples;
    cout << "Enter the number of home runs hit: ";
    cin >> homeRuns;
    cout << "Enter the number of at-bats: ";
    cin >> atBats;
    
    sluggingPercentage = ( singles + 2 * doubles + 3 * triples + 4 * homeRuns)
            / static_cast<double>(atBats);
    
    cout << "This player's slugging percentage is " << sluggingPercentage << "."
         << endl << endl;
    
    //NUMBER 3
    
    int num1, num2, num1a;
    
    cout << "Enter two numbers less than 1000: ";
    cin >> num1 >> num2;
    cout << left << setw(4) << "X = " << setw(5) << num1 << "Y = " << num2
         << endl;
    
    num1a = num1;
    num1 = num2;
    num2 = num1a;
    
    cout << setw(80) << setfill('-') << "-" << endl << setfill(' ');
    cout << setw(4) << "X = " << setw(5) << num1 << "Y = " << num2
         << endl << endl;
    
    //NUMBER 4
    
    int x, y;
    
    cout << "Enter a number: ";
    cin >> x;
    
    if (x == 4)
    {
        y = 4;
    }
    else if (x == 9)
    {
        y = 5;
    }
    else
    {
        y = 6;
    }
    
    cout << "If X is " << x << ", then Y is " << y << "." << endl
            << endl;
    
    //NUMBER 5
    
    double totalCost, cashInHand;
    int pennies, nickles, dimes, quarters, dollars;
    double realPennies, realNickles, realDimes, realQuarters, realDollars;
    
    cout << "Enter the total cost of this purchase: ";
    cin >> totalCost;
    cout << "How many pennies do you have?" << endl;
    cin >> pennies;
    cout << "How many nickles do you have?" << endl;
    cin >> nickles;
    cout << "How many dimes do you have?" << endl;
    cin >> dimes;
    cout << "How many dollars do you have?" << endl;
    cin >> dollars;
    
    realPennies = pennies * .01;
    realNickles = nickles * .05;
    realDimes = dimes * .1;
    realQuarters = quarters * .25;
    realDollars = dollars * 1.0;
    
    cashInHand = realPennies + realNickles + realDimes + realDollars +
            realQuarters;
    
    cout << "Your total cash in hand is $" << cashInHand << "." << endl;
    
    if (cashInHand < totalCost)
    {
        cout << "You do not have enough money for your purchase.";
    }
    else
    {
        cout << "You have enough money for your purchase.";
    }
    cout << endl << endl;
    
    //NUMBER 6
    
    double xx = 10.76;
    double yy = 1.50;
    const int mult = 100;
    
    double dif = xx-yy;
    cout << dif << endl;
    
    int intDif = static_cast<int>(dif*mult);
    
    cout << intDif << endl;
    cout << dif*mult << endl;
    
    return 0;
        
}

