/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on November 10, 2015, 11:37 AM
 */

#include <cstdlib>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

void partialOutput(int a[], int size)
{
    
}
void output(const vector<int>&v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

void input(int a[], int&size, int cap)
{
    int temp;
    do
    {
        cout << "Enter a number to add to the array (-1 to stop): ";
        cin >> temp;
        if(temp != -1)
        {
            a[cap] = temp;
            cap++;
        }
    }
    while(temp != -1 && size != cap);
    cout << "Array: ";
    output(a, cap);
}

vector<int> randomVector()
{
    srand(time(0));
    vector<int> v;
    int num;
    for(int i = 0; i < 10; i++)
    {
        num = rand()%100;
        v.push_back(num);
    }
    return v;
}

void randomArray(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        int num = rand()%100;
        a[i] = num;
    }
    output(a, size);
        
}

void minVal(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        int num = rand()%100;
        a[i] = num;
    }
    cout << "Array: ";
    output(a, size);    
    int min = 999999999;
    for(int i = 0; i < size; i++)
    {
        if(a[i] < min)
            min = a[i];
    }
    cout << "Minimum value: " << min << endl << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    int cap = 0;
    
    cout << "   NUMBER 1" << endl << endl;
    int size = 10;
    int a[size];
    cout << "Vector: ";
    output(randomVector());
    cout << "Array: ";
    randomArray(a, size);
    cout << endl;
    
    cout << "   NUMBER 2" << endl << endl;
    size = 5;
    int b[size];
    input(b, size, cap);
    cout << endl;
    
    cout << "   NUMBER 3" << endl << endl;
    size = 10;
    int c[size];
    input(c, size, cap);
    cout << endl;
    
    cout << "   NUMBER 4" << endl << endl;
    int d[size];
    minVal(d, size);
    
    cout << "   NUMBER 6" << endl << endl;
    
    
    
    return 0;
}

