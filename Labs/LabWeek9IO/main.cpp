/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 27, 2015, 11:39 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
        
    char input;
    string sentence;
    ofstream outfile;
    outfile.open ("output.dat");
    do
    {
        cout << "Enter a sentence (typing '@' ends the sentence input): " 
                << endl;
        getline(cin, sentence);
        int search = sentence.find('@');
        input = sentence[search];
        if(input == '@')
            outfile << sentence.substr(0,search) << endl;
        else
            outfile << sentence << endl;   
    }
    while(input != '@');
    outfile.close();

    //NUMBER 2
    
    char c;
    sentence = "";
    outfile.open ("output.dat");
    cout << "Enter a sentence (typing '@' ends the sentence input): " << endl;
    do
    {
        cin.get(c);
        if(c != '@')
        {
            sentence += c; 
        }
    }
    while(c != '@');
    outfile << sentence << endl;
    outfile.close();
        
    //NUMBER 3
    //cin - reads input until a space or new line is entered. It ignores the
    //      space and puts the new line into the buffer.
    //getline(cin,s) - reads input until a new line, and ignores the new line
    //                 character. "s" is a string containing the full input.
    //cin.get(c) - reads and stores a single character from input. "c" is that
    //             character.
    
    //NUMBER 4
    
    
    
    return 0;
}

