/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 15, 2015, 11:36 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

//NUMBER 1

double Power(double base, int exp)
{
    double num = base;
    for(int i = 0; i < exp; i++)
    {
        num = num * base;
    }
    return num;
}

//NUMBER 2

bool LeapYear(int year)
{
    if(year%4 == 0 && (year%100 != 0 || year%400 == 0))
        return true;
    else
        return false;
}

//NUMBER 3

int Factorial(int base)
{
    int total = base;
    if(base > 0)
    {
        for(int i = 0; i < base; i++)
        {
            if(i == 0)
            {
                total = total * 1;
            }
            else
            {
                total = total * i;
            }
        }    
    }
    else
    {
        total  = 1;
    }
    return total;
}

//NUMBER 4

void swapNum(double&num1, double&num2)
{
    double num1a = num1;
    num1 = num2;
    num2 = num1a;
}
//NUMBER 5

bool Pali(string word)
{
    for(int i = 0; i < word.size(); i++)
    {
        if(!(word[i] == '!' || word[i] == '@' || word[i] == '#' || word[i] == '$'
                || word[i] == '%' || word[i] == '^' || word[i] == '&'
                || word[i] == '*' || word[i] == '(' || word[i] == ')'
                || word[i] == '-' || word[i] == '_' || word[i] == '='
                || word[i] == '+' || word[i] == '`' || word[i] == '~'))   
        {
            if(word[i] != word[word.size()-1-i])
            {
                return false;
            }    
        }
        
    }
    return true;
}
/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 2 (CONT'D)
    
    int year;
    string response;
    cout << "NUMBER 2: " << endl;
    do
    {
        cout << "Input a year: ";
        cin >> year;
        if(LeapYear(year) == true)
            cout << year << " is a leap year." << endl;
        else
            cout << year << " is not a leap year." << endl;
        cout << "Check another year?" << endl;
        cin >> response;
    }
    while(tolower(response[0]) == 'y');
    cout << endl;
    
    // NUMBER 3
    
    int num;
    cout << "NUMBER 3: " << endl;
    cout << "Enter a number: " << endl;
    cin >> num; 
    cout << "The factorial of " << num << " is " << Factorial(num) << endl
            << endl;
    
    //NUMBER 5
    
    string word;
    cout << "NUMBER 5: " << endl;
    cout << "Enter a word: " << endl;
    cin >> word;
    if(Pali(word) == true)
    {
        cout << word << " is a palindrome." << endl;
    }
    else
    {
        cout << word << " is not a palindrome." << endl;
    }
    return 0;
}

