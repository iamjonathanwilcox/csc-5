/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 6, 2015, 12:00 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    for(int i = 0; i < 10; i++)
    {
        int x = rand()%101, y = rand()%101;
        if(x < y)
        {
            cout << x << " is less than " << y << "." << endl;
        }
        else if(x > y)
        {
            cout << x << " is greater than " << y << "." << endl;
        }
        else
        {
            cout << x << " is equal to " << y << "." << endl;
        }
    }
    cout << endl;
    
    //NUMBER 2
    
    ifstream infile;
    infile.open("file.txt");
    string data;
    int wordCount = 0;
    while (infile >> data)
    {
        cout << data << " ";
        wordCount = wordCount + data.size();
    }
    cout << endl;
    cout << "This file has " << wordCount << " characters." << endl << endl;
    
    //NUMBER 3
    
    ofstream outfile;
    outfile.open("size.txt");
    outfile << wordCount;
    outfile.close();
    
    //NUMBER 4
    
    int n;
    cout << "Input a number: ";
    cin >> n;
    double x = 0;
    double s;
    do
    {
        s = 1.0 / (1 + n * n);
        n++;
        x = x + s;
    }
    while(s > 0.01);    
    cout << s << " " << n << " " << x << " " << endl << endl;    \

    cout << "Input a number: ";
    cin >> n;
    s = 1.0 / (1 + n * n);
    x = 0;
    while(s > 0.01 && s == s)
    {
        s = 1.0 / (1 + n * n);
        n++;
        x = x + s;
    }
    cout << s << " " << n << " " << x << " " << endl << endl;
    return 0;
}

