/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on November 17, 2015, 11:30 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

void swap(int& num1, int& num2)
{
    int num1a = num1;
    num1 = num2;
    num2 = num1a;
}

void bubbleSort(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size-1; j++)
        {
            if(a[i] > a[j+1])
                swap(a[j], a[j+1]);
        }
    }
}

void selectionSort(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        int min = -999999999;
        int loc = -1;
        for(int j = 0; j < size; j++)
        {
            if(a[j] < min)
            {
                min = a[j];
                loc = j;
            }
        }
    }
}
/*
 * 
 */
int main(int argc, char** argv) {

    return 0;
}

