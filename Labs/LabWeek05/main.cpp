/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 29, 2015, 11:32 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    string response;
    double balance, interest1 = 0, interest2 = 0, interestSum, minPay;
    cout << "Would you like to view account details? (Y/N)" << endl ;
    cin >> response;
    
    while(response == "Y")
    {
        
        cout << "Enter the account balance: ";
        cin >> balance;

        if(balance <= 1000)
        {
            interest1 = (( balance * 100 ) * 1.5 ) / 100;    
        }
        else
        {
            interest1 = (100000 * 1.5 ) / 100 ;
            interest2 = ( balance - 1000 ) * 0.01;
        }
        
        interestSum = interest1 + interest2;
        
        if(interestSum <= 10.00)
        {
            minPay = interestSum;
        }
        else
        {
            minPay = ( interestSum * 0.10 );
        }
        
        cout << "The total amount due is $" << interestSum << "." << endl
             << "The minimum payment due is $" << minPay << "." << endl << endl
             << "Would you like to change the account balance? (Y/N)" << endl;
        cin >> response;
            
    }
    cout << endl;
    
    //NUMBER 2
    
    cout << "Would you like to play a game? (Y/N)" << endl;
    cin >> response;
    
    int toothpicks, choice, compChoice;
    while(response == "Y")
    {
        toothpicks = 23;
        do
        {
        cout << "There are " << toothpicks << " toothpicks. "
             << "You can draw 1, 2, or 3 toothpicks." << endl
             << "How many toothpicks do you draw? ";
        cin >> choice;
        
            if(choice == 1)
            {
                toothpicks--;
            }
            else if(choice == 2)
            {
                toothpicks = toothpicks - 2;
            }
            else if(choice == 3)
            {
                toothpicks = toothpicks - 3;
            }
            else
            {
                if(choice != 1 || choice != 2 || choice != 3)
                {
                    cout << "Not a valid input. Choose 1, 2, or 3: ";
                    cin >> choice;
                }
                
            }
            
            if(toothpicks == 0)
            {
                cout << "You lose!" << endl;
            }
            
            cout << "The total remaining toothpicks is " << toothpicks << "."
                 << endl;
            
            if(toothpicks > 4)
            {
                compChoice = choice;
                toothpicks = toothpicks - compChoice;
            }
            else if(toothpicks >= 2 && toothpicks <= 4)
            {
                int math = toothpicks;
                do
                {
                    compChoice = 0;
                    compChoice++;
                    math--;
                }
                while(math > 1);
                toothpicks = toothpicks - compChoice;
            }
            else
            {
                compChoice = 1;
                toothpicks = toothpicks - compChoice;
            }
        
            cout << "The computer draws " << compChoice << " toothpicks." << endl;
            cout << "The number of remaining toothpicks is " << toothpicks << "."
                 << endl;
        
            if(toothpicks == 0)
            {
                cout << "You win!" << endl;
            }
            
        }
        while(toothpicks != 0);
        
        cout << "Would you like to play again? (Y/N)" << endl;
        cin >> response;
                     
    }
    
            
            
    return 0;
}

