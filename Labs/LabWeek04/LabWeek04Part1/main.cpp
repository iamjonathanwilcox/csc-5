/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 22, 2015, 11:30 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    int grade ;
    cout << "Enter a numerical grade value: ";
    cin >> grade;
    
    if(grade > 99)
    {
        cout << grade << " is an A+.";
    }
    else if (grade > 89 && grade < 100)
    {
        cout << grade << " is an A.";
    }
    else if(grade > 79 && grade < 90)
    {
        cout << grade << " is a B.";
    }
    else if(grade > 74 && grade < 80)
    {
        cout << grade << " is a C.";
    }
    else if(grade > 69 && grade < 75)
    {
        cout << grade << " is a D.";
    }
    else
    {
        cout << grade << " is an F.";
    }
    cout << endl << endl;
    
    //NUMBER 2A
    
    int x = 0;
    while(x < 10)
    {
        cout << x << endl;
        x++;
    }
    cout << endl << endl;
    
    //NUMBER 2B
    
    string response;
    cout << "Enter a number? Y/N: ";
    cin >> response;
    
    while(response == "Y")
    {
        cout << "Enter a number: ";
        int num;
        cin >> num;
        if(num > 99)
        {
            cout << num << " is an A+." << endl;
        }
        else if (num > 89 && num < 100)
        {
            cout << num << " is an A." << endl;
        }
        else if(num > 79 && num < 90)
        {
            cout << num << " is a B." << endl;
        }
        else if(num > 74 && num < 80)
        {
            cout << num << " is a C." << endl;
        }
        else if(num > 69 && num < 75)
        {
            cout << num << " is a D." << endl;
        }
        else
        {
            cout << num << " is an F." << endl;
        }
        cout << "Enter another number? Y/N: ";
        cin >> response;
    }
    cout << endl << endl;
    
    //NUMBER 3
    
    int input;
    for(int i = 0; i < 10; i++)
    {
        cout << "Enter a number: ";
        cin >> input;
        cout << input << endl;
    }
    cout << endl << endl;
    
    //NUMBER 4
    
    string grade2;
    cout << "Enter a letter grade: ";
    cin >> grade2; 
    
    if(grade2 == "A+" || grade2 == "a+")
    {
        cout << "The numerical score range is 100+.";
    }
    else if(grade2 == "A" || grade2 == "a")
    {
        cout << "The numerical score range is 93 - 100.";
    }
    else if(grade2 == "A-" || grade2 == "a-")
    {
        cout << "The numerical score range is 90 - 92.9.";
    }
    else if(grade2 == "B+" || grade2 == "b+")
    {
        cout << "The numerical score range is 87 - 89.9.";
    }
    else if(grade2 == "B" || grade2 == "b")
    {
        cout << "The numerical score range is 83 - 86.9.";
    }
    else if(grade2 == "B-" || grade2 == "b-")
    {
        cout << "The numerical score range is 80 - 82.9.";
    }
    else if(grade2 == "C+" || grade2 == "c+")
    {
        cout << "The numerical score range is 77 - 79.9.";
    }
    else if(grade2 == "C" || grade2 == "c")
    {
        cout << "The numerical score range is 73 - 76.9.";
    }
    else if(grade2 == "C-" || grade2 == "c-")
    {
        cout << "The numerical score range is 70 - 72.9.";
    }
    else if(grade2 == "D+" || grade2 == "d+")
    {
        cout << "The numerical score range is 67 - 69.9.";
    }
    else if(grade2 == "D" || grade2 == "d")
    {
        cout << "The numerical score range is 63 - 66.9.";
    }
    else if(grade2 == "D-" || grade2 == "D-")
    {
        cout << "The numerical score range is 60 - 62.9.";
    }
    else if(grade2 == "F")
    {
        cout << "The numerical score range is 0 - 59.9.";
    }
    else
    {
        cout << "Your input is not a test score.";
    }
    cout << endl << endl;

            
    return 0;

}

