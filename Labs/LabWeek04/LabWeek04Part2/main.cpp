/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 24, 2015, 11:34 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    string grade;
    cout << "Enter a letter grade: ";
    cin >> grade;
    if(grade.size() == 2)
    {
        if(grade.substr(0,1) == "A")
        {
            if(grade.substr(1) == "+")
            {
                cout << "Your grade is a 100.0." << endl << endl;
            }
            else if(grade.substr(1) == "-")
            {
                cout << "Your grade is a 90.0." << endl << endl;
            }
            else
            {
                cout << "Whatever you entered was not a grade." << endl << endl;
            }
        }
        else if(grade.substr(0,1) == "B")
        {
            if(grade.substr(1) == "+")
            {
                cout << "Your grade is a 87.0." << endl << endl;
            }
            else if(grade.substr(1) == "-")
            {
                cout << "Your grade is a 80.0." << endl << endl;
            }
            else
            {
                cout << "Whatever you entered was not a grade." << endl << endl;
            }
            
        }
        else
        {
            cout << "You did not enter a valid grade." << endl << endl;
        }
         
    }
    else
    {
        if(grade.substr(0,1) == "A")
        {
            cout << "Your grade is a 93.0" << endl << endl;
        }
        if(grade.substr(0,1) == "B")
        {
            cout << "Your grade is a 83.0" << endl << endl;
        }
        else
        {
            cout << "You did not enter a valid grade." << endl << endl;
        }
    }
    
    
    //NUMBER 3 && NUMBER 4
    int year;
    cout << "Enter a year: ";
    cin >> year;
    if(year%4 == 0 && year%100 == 0 && year%400 == 0)
    {
        cout << year << " is a leap year." << endl << endl;
    }
    else
    {
        cout << year << " is not a leap year." << endl << endl;
    }
      
    //NUMBER 5
    
    int actualNum, guess;
    string yes;
    cout << "Want to play a guessing game? (Y/N) ";
    cin >> yes;
    
    while (yes == "Y" || yes == "y")
    {
        actualNum = rand() % 101;
        cout << "Enter a number 1-100: ";
        cin >> guess;
        
        if(guess != actualNum)          
        {
            for(int i = 0; i < 9; i++)
            {
                if(guess < actualNum)
                {
                    cout << "Incorrect! You guessed too low. Guess again: " ;
                    cin >> guess;
                }
                else if(guess > actualNum)
                {
                    cout << "Incorrect! You guessed too high. Guess again: ";
                    cin >> guess;
                }

            }
        
            
            
        }
       
        
        
        cout << "You win! Would you like to play again? (Y/N)";
        cin >> yes;
                
    }
    
    
    return 0;
}

