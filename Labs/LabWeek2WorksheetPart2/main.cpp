/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/10/2015
* Lab Week 2 (pt 2)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Number 1
    string name;
    cout << "Hello, my name is Jonathan!" << endl;
    cout << "What is your name?" << endl;
    cin >> name;
    cout << "Hello, " << name << ". I am glad to meet you." << endl << endl;
    
    //Number 2
    int a = 5;
    int a1;
    int b = 10;
    cout << "a: " << a << " " << "b: " << b << endl;
    a1 = a;
    a = b;
    b = a1;
    cout << "a: " << a << " " << "b: " << b << endl << endl;
    
    //Number 3
    int meters;
    cout << "Enter measurement in meters: ";
    cin >> meters;
    double miles = meters * 1609.344;
    cout << meters << " meters is equivalent to " << miles << " miles." << endl << endl;
    
    //Number 4
    int x;
    cout << "Enter a whole number: ";
    cin >> x;
    x = x + 5;
    cout << x << endl << endl;
    
    //Number 5a
    int testScore1, testScore2, testScore3, average;
    cout << "Enter first test score: ";
    cin >> testScore1;
    cout << "Enter second test score: ";
    cin >> testScore2;
    cout << "Enter third test score: ";
    cin >> testScore3;
    average = (testScore1 + testScore2 + testScore3) / 3;
    cout << "Your average test score is " << average << "." << endl << endl;
    
    //Number 5b
    int testScore01, testScore02, testScore03, average2;
    cout << "Enter test scores: " << endl;
    cin >> testScore01 >> testScore02 >> testScore03;
    average2 = (testScore01 + testScore02 + testScore03) / 3;
    cout << "Your average test score is " << average2 << "." << endl << endl;
    
    //Number 6
    int integer;
    cout << "Enter a whole number between 1 and 10000: ";
    cin >> integer;
    cout << setw(1) << setprecision(1) << "The number you entered was " << integer << endl ;
    //WOW I'M AT SUCH A DAMN LOSS
    
    return 0;
}

