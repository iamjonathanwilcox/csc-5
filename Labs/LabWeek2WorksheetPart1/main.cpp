/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 8, 2015, 11:28 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string name;
    cout << "Hello, my name is Jonathan!" << endl;
    cout << "What is your name?" << endl;
    cin >> name;
    cout << "Hello, " << name << ". I am glad to meet you.";
            
    return 0;
}

