/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on November 5, 2015, 11:34 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

void output(const vector<int>&v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
}


//NUMBER 2
void newVector(vector<int>&v)
{
    int counter = 0;
    srand(time(0));
    do
    {
        int num = rand()%50+50;
        v.push_back(num);
        counter++;
    }
    while(counter < 50);
    output(v);
}

//NUMBER 3
bool isPresent(const vector<int>&v, int val)
{
    for(int i = 0; i < v.size(); i++)
    {
        if(val == v[i]) return true;
    }
    return false;
}

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    vector<int> v;
    ifstream infile;
    string name;
    
    cout << "Enter name of file to be transcribed: ";
    cin >> name;
    
    infile.open(name.c_str());
    if(infile.fail())
    {
        cout << "File failed.\n";
    }
    else
    {
        int val;
        while(infile >> val)
        {
            v.push_back(val);
        }
    }
    infile.close();
    
    output(v);
    cout << endl << endl;
    
    //NUMBER 3
    int num;
    cout << "Input a number to see if it is inside the vector: ";
    cin >> num;
    if(isPresent(v, num) == true)
        cout << num << " is inside the vector." << endl << endl;
    else
        cout << num << " is not inside the vector." << endl << endl;
    
    return 0;
}

