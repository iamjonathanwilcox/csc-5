/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on October 27, 2015, 11:23 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

//NUMBER 3

void output(vector<int>&number3)
{
    for(int i = 0; i < number3.size(); i++)
    {
        cout << number3[i] << " ";
    }
    cout << endl;
}

//NUMBER 4

void number4(vector<int>&numbers)
{
    cout << "Before function: " << endl;
    output(numbers);
    int temp;
    do
    {
        cout << "Enter a number (-1 to stop): ";
        cin >> temp;
        numbers.push_back(temp);
    }
    while(temp != -1);
    cout << endl << "After function: " << endl;
    output(numbers);
    cout << endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    vector<int> number1;
    number1.push_back(3);
    number1.push_back(6);
    number1.push_back(8);
    cout << endl;
    
    //NUMBER 2
    
    int temp;
    vector<int> numbers;
    do
    {
        cout << "Enter a number: ";
        cin >> temp;
        numbers.push_back(temp);
    }
    while(temp != -1);
    cout << endl;

    return 0;
}

