/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 8, 2015, 10:44 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //Ask the user what year they were born
    //Verify the year with the user
    int yearBorn;
    cout << "What year were you born? ";
    
    //endl variable forces output to go to next line
    cout << endl;
    cin >> yearBorn;
    cout << "You were born in year " << yearBorn << ".";
    
    return 0;
}

