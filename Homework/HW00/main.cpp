/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/8/2015
* HW: Homework 0
* Problem: Create a program that outputs your favorite food.
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "My favorite food is sushi. What you gonna do about it, homes?";
    
    return 0;
}

