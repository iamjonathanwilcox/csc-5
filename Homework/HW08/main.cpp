/*/
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 11/21/2015  
* HW: 08
* Problem: ALL (annotated in main function comments and output)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;
/**
 * This function checks whether or not a number is present inside a vector
 * @param v - vector to be evaluated
 * @param num - number to search for
 * @return 
 */
bool isPresent(const vector<int>& v, int num)
{
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] == num) return true;
    }
    return false;
}

/**
 * This function checks an array to see if a specified value is present
 * @param a - array to be evaluated
 * @param size - size of array
 * @param val - value to check for
 * @return - whether or not the number is present
 */
bool isPresent(const int a[], int size, int val)
{
    for(int i = 0; i < size; i++)
    {
        if(a[i] == val) return true;
    }
    return false;
}

/**
 * This function swaps two integer values
 * @param num1 - The first number to be swapped
 * @param num2 - The second number
 */
void swap(int& num1, int& num2)
{
    int num1a = num1;
    num1 = num2;
    num2 = num1a;
}

/**
 * This function creates a vector of 10 random integers.
 * @return - completed vector
 */
vector<int> randomVector()
{
    srand(time(0));
    vector<int> v;
    int num;
    for(int i = 0; i < 10; i++)
    {
        num = rand()%100;
        v.push_back(num);
    }
    return v;
}

/**
 * This function fills an array with random values
 * @param a - The array to be filled
 * @param size - The size of the array
 */
void randomArray(int a[], int size)
{
    int num;
    for(int i = 0; i < size; i++)
    {
        num = rand()%100;
        a[i] = num;
    }    
}

/**
 * This function outputs all numbers within a vector of integers.
 * @param v - the vector of integers to be output
 */
void output(const vector<int>&v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * This function outputs an array of integers
 * @param a - array to be output
 * @param size - size of the array to be output
 */
void output(int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * This function outputs an array of characters 
 * @param a - array to be output
 * @param size - size of the array
 */
void output(char a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * This function removes the value at a specified location from a vector
 * @param v - vector to remove value from
 * @param loc - location of the value to be removed
 */
void delElement(vector<int>& v, int loc)
{
    swap(v[loc], v[v.size()-1]);
    v.pop_back();
}

/**
 * This function removes the value at a specified location from a vector and
 * keeps the remaining vector in order
 * @param v - vector to remove value from
 * @param num - value to be removed
 */
void delElement_sameOrder(vector<int>& v, int num)
{
    if(isPresent(v, num) != true)
    {
        cout << "NUMBER NOT FOUND." << endl;
    }
    else
    {
        for (int i = 0; i < v.size(); i++)
        {
            if(num == v[i])
            {
                for (int j = i; j < v.size(); j++)
                {
                    v[j] = v[j+1];
                }
                v.pop_back();
            }
        }
    }
}

/**
 * This function removes the value at a specified location from an array
 * @param a - array to remove value from
 * @param size - size of the array
 * @param loc - location of the value to be removed from array
 */
void delElement(int a[], int&size, int loc)
{
    swap(a[loc], a[size-1]);
    size--;
}

/**
 * This function removes the value at a specified location from an array while
 * keeping the array in the same order.
 * @param a - array to be evaluated
 * @param size - size of the array
 * @param val - value to be removed
 */
void delElement_sameOrder(int a[], int&size, int val)
{
    if(isPresent(a, size, val) != true)
    {
        cout << "NUMBER NOT FOUND." << endl;
    }
    else
    {
        for(int i = 0; i < size; i++)
        {
            if(val == a[i])
            {
                for(int j = i; j < size; j++)
                {
                    swap(a[j], a[j+1]);
                }
                size--;
            }
        }
    }
}

/**
 * This function removes a specified character from an array while maintaining
 * the order of the array
 * @param a - Array to be evaluated
 * @param size - size of array
 * @param val - Value to be removed
 */
void delElement_sameOrder(char a[], int&size, int loc)
{
    for(int i = 0; i < size; i++)
    {
        if(loc == i)
        {
            for(int j = i; j < size; j++)
            {
                swap(a[j], a[j+1]);
            }
            size--;
        }
    }
}

/**
 * This function generates a random value for a ticket number
 * @return - random number
 */
int ticketNum()
{
    srand(time(0));
    int num = rand()%39+1;
    return num;
}

/**
 * This function checks for duplicate characters within an array and removes
 * any repeated ones.
 * @param a - array to be evaluated
 * @param size - maximum size of array
 * @param cap - capacity of partial array
 */
void delete_repeats(char a[], int&size)
{
    char temp;
    for(int i = 0; i < size; i++)
    {
        for(int j = i+1; j < size; j++)
        {
            if(a[i] == a[j])
            {
                delElement_sameOrder(a, size, j);
            }
        }
    }
}
/*
 * 
 */
int main(int argc, char** argv) {

    srand(time(0));
    vector<int> v = randomVector();
    int loc = rand()%10;
    int size = 10, a[size];
    int num;
    
    // NUMBER 1
    cout << "   NUMBER 1:" << endl << endl;
    cout << "Original vector: ";
    output(v);
    delElement(v, loc);
    cout << "New vector: ";
    output(v);
    cout << endl;
    
    //NUMBER 2
    cout << "   NUMBER 2:" << endl << endl;
    randomArray(a, size);
    cout << "Original array: ";
    output(a, size);
    delElement(a, size, loc);
    cout << "New array: ";
    output(a, size);
    cout << endl;
    
    //NUMBER 3 + NUMBER 4
    cout << "   NUMBER 3 (and NUMBER 4):" << endl << endl;
    cout << "Original vector: ";
    output(v);
    cout << "Input value to be removed from vector: ";
    cin >> loc;
    delElement_sameOrder(v, loc);
    cout << "New vector: ";
    output(v);
    cout << endl;
    
    //NUMBER 5
    cout << "   NUMBER 5:" << endl << endl;
    int b[size];
    randomArray(b, size);
    cout << "Original array: ";
    output(b, size);
    cout << "Input location of value to be removed from array: ";
    cin >> loc;
    delElement_sameOrder(b, size, loc);
    cout << "New array: ";
    output(b, size);
    cout << endl;
    
    //NUMBER 6
    cout << "   NUMBER 6:" << endl << endl;
    cout << "Number to search for in vector: ";
    cin >> num;
    cout << "Vector: ";
    output(v);
    if(isPresent(v, num) == true)
    {
        cout << num << " is in the vector.";
    }
    else
    {
        cout << num << " is not in the vector.";
    }
    cout << endl << endl;
    
    //NUMBER 7
    cout << "   NUMBER 7:" << endl << endl;
    vector<int> v2;
    int choice = 1;
    do
    {
        cout << "Number to add to the vector: ";
        cin >> num;
        if(isPresent(v2, num) != true)
        {
            v2.push_back(num);
        }
        else
        {
            cout << "That number is already in the vector." << endl;
        }
        cout << "Do you wish to enter another number?" << endl
                << "    1. Yes" << endl
                << "    2. No" << endl
                << "Your choice: ";
        cin >> choice;
    }while(choice == 1);
    cout << "Full vector: ";
    output(v2);
    cout << endl;
    
    //NUMBER 8
    cout << "   NUMBER 8:" << endl << endl;
    choice = 0;
    string name = "";
    num = 0;
    vector<string> names;
    vector<int> tickets;
    do
    {
        num = ticketNum();
        cout << "Enter the name you would like to assign a ticket: ";
        cin >> name;
        names.push_back(name);
        if(isPresent(tickets, num) != true)
        {
            tickets.push_back(num);
        }
        else
        {
            while(isPresent(tickets, num) == true)
            {
                num = ticketNum();
            }
            tickets.push_back(num);
        }
        cout << "Enter -1 to stop, 0 to add another name: ";
        cin >> choice;
    }
    while(choice != -1 || names.size() >= 40);
    cout << endl;
    
    //NUMBER 9
    cout << "   NUMBER 9:" << endl << endl;
    size = 10;
    int cap = 4;
    char c[10];
    c[0] = 'a';
    c[1] = 'b';
    c[2] = 'a';
    c[3] = 'c';
    cout << "Original Array: ";
    output(c, cap);
    delete_repeats(c, cap);
    cout << "Fixed Array: ";
    output(c, cap);
   
    return 0;
}

