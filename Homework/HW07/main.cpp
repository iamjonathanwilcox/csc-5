/*/
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 11/7/2015  
* HW: 07
* Problem: ALL (annotated in comments)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

/**
 * This function creates the vector and then returns it to be used in other
 * functions
 * @param file - the file to read the vector from
 * @return - the compiled vector to be run through other functions
 */
vector<int> makeVector(string file)
{
    vector<int> v;
    ifstream infile;
    infile.open(file.c_str());
    if(infile.fail())
    {
        cout << "File failed." << endl;
    }
    else
    {
        int temp;
        while(infile >> temp)
        {
            v.push_back(temp);   
        }
    }
    infile.close();
    return v;
}

/**
 * This function makes a string out of text from a file.
 * @param file - the file to be read
 * @return - the full string from file
 */
string makeString(string file)
{
    string full = "";
    ifstream infile;
    infile.open(file.c_str());
    if(infile.fail())
    {
        cout << "File failed." << endl;
    }
    else
    {
        while(!infile.eof())
            getline(infile,full);
    }
    infile.close();
    return full;
}

/**
 * This function combines two files into one string.
 * @param file - The first file
 * @param file2 - The second file
 * @return - The combined string
 */
string makeString(string file, string file2)
{
    string full = "";
    ifstream infile;
    infile.open(file.c_str());
    if(infile.fail())
    {
        cout << "File failed." << endl;
    }
    else
    {
        while(!infile.eof())
            getline(infile,full);
    }
    infile.close();
    infile.open(file2.c_str());
    if(infile.fail())
    {
        cout << "File failed." << endl;
    }
    else
    {
        while(!infile.eof())
            getline(infile,full);
    }
    infile.close();
    return full;
}

/**
 * This function outputs all numbers within a vector of integers.
 * @param v - the vector of integers to be output
 */
void output(const vector<int>&v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * This function outputs all even numbers within a vector of integers.
 * @param v - the vector of integers to be evaluated
 */
void outputEvens(const vector<int>&v)
{
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i]%2 == 0) cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * This function determines the median number given a vector of integers.
 * @param v - vector to be evaluated
 * @return - median number
 */
int outputMedian(const vector<int>&v)
{
    int median = (v[(v.size() / 2)]);
    return median; 
}

/**
 * This function evaluates the biggest number within a vector of integers.
 * @param v - vector of integers to be evaluated
 * @return - the biggest integer inside vector v
 */
int evalBig(const vector<int>&v)
{
    int biggest = -999999999;
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] > biggest)
            biggest = v[i];
    }
    return biggest;
}

/**
 * This function removes excess spaces from a string.
 * @param full - the full string to be evaluated
 * @return - the fixed string
 */
string removeSpace(string full)
{
    string fixed = "";
    for(int i = 0; i < full.size(); i++)
    {
        if(full[i] != ' ')
            fixed += full[i];
        else if(full[i] == ' ' && full[i+1] != ' ')
            fixed += full[i];
    }
    return fixed;
}

/**
 * This function writes a string to a new file
 * @param full - the string to be written to a new file
 */
void toFile_string(string full)
{
    string file;
    cout << "Choose the name you wish to give the new file: ";
    cin >> file;
    ofstream outfile;
    outfile.open (file.c_str());
    outfile << full;
    outfile.close();
}

/**
 * This function evaluates the smallest number within a vector of integers.
 * @param v - vector of integers to be evaluated
 * @return - the smallest integer inside vector v
 */
int evalSmall(const vector<int>&v)
{
    int smallest = 999999999;
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] < smallest)
            smallest = v[i];
    }
    return smallest;
}

/**
 * This function pulls integers from a file, stores them into a vector, and then
 * evaluates the biggest and smallest integers.
 * @param file - the name of the file to be evaluated.
 */ 
void evalFile(string file)
{
    vector<int> v = makeVector(file);
    cout << "Full set: ";
    output(v);
    cout << "Biggest number: " << evalBig(v) << endl;
    cout << "Smallest number: " << evalSmall(v) << endl << endl;
}

/**
 * This function evaluates a file to determine how many even numbers a file
 * contains.
 * @param file - The file to be evaluated
 */
void evalFile2(string file)
{
    vector<int> v = makeVector(file);
    cout << "Full set: ";
    output(v);
    cout << "Even numbers: ";
    outputEvens(v);
    cout << endl;
}

/**
 * This function determines the median number in a vector of integers pulled
 * from a file
 * @param file - the file to be evaluated
 */
void evalFile3(string file)
{
    vector<int> v = makeVector(file);
    cout << "Full set: ";
    output(v);
    cout << "Median number: " << outputMedian(v) << endl << endl;
}

/**
 * This function takes a string from a file (determined by user) and removes
 * any excess spaces from the string, then writes the fixed string to a new
 * file named by the user.
 * @param file - the file to be read from initially
 */
void evalFile4(string file)
{
    string full = makeString(file);
    cout << full << endl;
    string fixed = removeSpace(full);
    cout << fixed << endl << endl;
    toFile_string(full);
    cout << "The fixed string has been saved to a new file." << endl << endl;
}

/**
 * This function takes in two files, combines them into one string, and outputs
 * that string to a new file.
 * @param file - The first file
 * @param file2 - the second file
 */
void evalFiles(string file, string file2)
{
    string full = makeString(file,file2);
    toFile_string(full);
    cout << file << " and " << file2 << " have been combined and saved to a new"
            "file." << endl << endl;    
}

/**
 * This function allows the user to choose a new file
 * @param file - the current stored value for the file name, to be altered
 */
void chooseFile(string&file)
{
    file = "";
    cout << "Enter the file name you wish to read: ";
    cin >> file;
    cout << endl;
}

/**
 * This function allows the user to choose two files to be processed together.
 * @param file - the first file
 * @param file2 - the second file
 */
void chooseFile(string&file, string&file2)
{
    file = "";
    cout << "Enter the first file name you wish to read: ";
    cin >> file;
    cout << "Enter the second file name you wish to read: ";
    cin >> file2;
    cout << endl;
}

/**
 * This function creates a vector of 10 random integers.
 * @return - completed vector
 */
vector<int> randomVector()
{
    srand(time(0));
    vector<int> v;
    int num;
    for(int i = 0; i < 10; i++)
    {
        num = rand()%100;
        v.push_back(num);
    }
    cout << "Numbers in vector: ";
    output(v);
    cout << endl;
    return v;
}

/**
 * This function creates a vector of 20 random integers
 * @return - completed vector
 */
vector<int> randomVector2()
{
    srand(time(0));
    vector<int> v;
    int num;
    for(int i = 0; i < 20; i++)
    {
        num = rand()%100;
        v.push_back(num);
    }
    cout << "Numbers in vector: ";
    output(v);
    return v;
}

/**
 * This function outputs all odd numbers within a vector of integers
 * @param v - vector to be available
 */
void oddNumbers(const vector<int>&v)
{
    cout << "Odd numbers in vector: ";
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i]%2 != 0)
        {
            cout << v[i] << " ";
        }
    }
    cout << endl << endl;
}

/**
 * This function finds the highest number within a vector of integers
 * @param v - vector to be evaluated
 */
void maxVal(const vector<int>&v)
{
    int max = -999999999;
    for(int i = 0; i < v.size(); i++)
    {
        if(v[i] > max)
            max = v[i];
    }
    cout << "Max number in vector: " << max << endl << endl;
    
}

/*
 * 
 */
int main(int argc, char** argv) {

    string file = "data.dat", file2 = "";
    cout << "   NUMBER 1:" << endl << endl;
    evalFile(file);
    cout << "   NUMBER 2:" << endl << endl;
    chooseFile(file);
    evalFile(file);
    cout << "   NUMBER 3:" << endl << endl;
    chooseFile(file);
    evalFile2(file);
    cout << "   NUMBER 4:" << endl << endl;
    chooseFile(file);
    evalFile3(file);
    cout << "   NUMBER 5:" << endl << endl;
    chooseFile(file);
    evalFile4(file);
    cout << "   NUMBER 6:" << endl << endl;
    chooseFile(file, file2);
    evalFiles(file, file2);
    cout << "   NUMBER 7:" << endl << endl;
    vector<int> v = randomVector();
    cout << "   NUMBER 8:" << endl << endl;
    oddNumbers(v);
    cout << "   NUMBER 9:" << endl << endl;
    v = randomVector2();
    maxVal(v);
    cout << "   NUMBER 10:" << endl << endl;
    cout << "Numbers in vector: " << output(v);
    return 0;
}

