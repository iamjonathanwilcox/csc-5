/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/17/15
* HW: 02
* Problem: Number 4
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string name, name2, food, adjective, color, animal;
    int number;
    
    cout << "Enter a name: ";
    cin >> name;
    cout << "Enter another name: ";
    cin >> name2;
    cout << "Enter a food: ";
    cin >> food;
    cout << "Enter a number between 100 and 120: ";
    cin >> number;
    cout << "Enter an adjective: ";
    cin >> adjective;
    cout << "Enter a color: ";
    cin >> color;
    cout << "Enter an animal: ";
    cin >> animal;
    cout << endl << endl;
    
    cout << "Dear " << name << "," ;
    cout << endl << endl << "I am sorry that I am unable to turn in my homework"
         << "at this time. First, I ate rotten " << food << "," << endl
         << "which made me turn " << color << " and extremely ill. I came down"
         << "with a fever of " << number << ". Next, my " << endl << adjective
         << " pet " << animal << " must have smelled the remains of the "
         << food << " on my homework because" << endl << " he ate it. I am"
         << "currently rewriting my homework and hope you will accept it late."
         << endl << endl << "Sincerely," << endl << name2 ;
    
    return 0;
}

