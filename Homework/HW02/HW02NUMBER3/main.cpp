/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/17/15
* HW: 02
* Problem: Number 3
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string name1, name2, name3;
    int firstScore1, firstScore2, firstScore3, secondScore1, secondScore2,
        secondScore3;
    int thirdScore1, thirdScore2, thirdScore3, fourthScore1, fourthScore2,
        fourthScore3;
    double average1, average2, average3, average4;
    
    cout << "Person 1: What is your name? " << endl;
    cin >> name1;
    cout << "Enter your quiz scores: "<< endl;
    cin >> firstScore1 >> secondScore1 >> thirdScore1 >> fourthScore1;
    
    cout << endl << "Person 2: What is your name? " << endl;
    cin >> name2;
    cout << "Enter your quiz scores: "<< endl;
    cin >> firstScore2 >> secondScore2 >> thirdScore2 >> fourthScore2;
    
    cout << endl << "Person 3: What is your name? " << endl;
    cin >> name3;
    cout << "Enter your quiz scores: "<< endl;
    cin >> firstScore3 >> secondScore3 >> thirdScore3 >> fourthScore3;
    
    average1 = ( firstScore1 + firstScore2 + firstScore3 ) / 3.0;
    average2 = ( secondScore1 + secondScore2 + secondScore3 ) / 3.0;
    average3 = ( thirdScore1 + thirdScore2 + thirdScore3 ) / 3.0;
    average4 = ( fourthScore1 + fourthScore2 + fourthScore3 ) / 3.0;
      
    cout << left << setw(10) << "name" << setw(8) << "Quiz 1" << setw(8)
         << "Quiz 2" << setw(8) << "Quiz 3" << setw (8) << "Quiz 4"
         << endl << setw(10) << "----" << setw(8) << "------" << setw(8)
         << "------" << setw(8) << "------" << setw (8) << "------"   
         << endl << setw(10) << name1 << setw(8) << firstScore1 << setw(8)
         << secondScore1 << setw(8) << thirdScore1 << setw (8) << fourthScore1 
         << endl << setw(10) << name2 << setw(8) << firstScore2 << setw(8)
         << secondScore2 << setw(8) << thirdScore2 << setw (8) << fourthScore2
         << endl << setw(10) << name3 << setw(8) << firstScore3 << setw(8)
         << secondScore3 << setw(8) << thirdScore3 << setw (8) << fourthScore3            
         << endl << endl
         << setw(10) << "Average: " << setw(8) << average1 << setw(8)
         << average2 << setw(8) << average3 << setw(8) << average4 ;
            
    return 0;
}

