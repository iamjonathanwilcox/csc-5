/*/
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 12/08/2015
* HW: 10
* Problem: ALL (annotated in comments and output)
* I certify this is my own work and code
*/
#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

//NUMBER 1
struct Time
{
    int seconds;
    int minutes;
    int hours;
};

//NUMBER 2
class Time1
{
public:
    int seconds;
    int minutes;
    int hours;
    Time1();
    Time1(int, int, int);
};

Time1::Time1()
{
    seconds = 0;
    minutes = 0;
    hours = 0;
}

Time1::Time1(int seconds, int minutes, int hours)
{
    this->seconds = seconds;
    this->minutes = minutes;
    this->hours = hours;
}

class Time2
{
private:
    int seconds;
    int minutes;
    int hours;
public:
    //NUMBER 7 - CONSTRUCTORS
    Time2();
    Time2(int, int, int);
    void setTime(int, int, int);
    void outputTime(ostream&);
};

//NUMBER 7
Time2::Time2()
{
    seconds = 0;
    minutes = 0;
    hours = 0;
}

//NUMBER 7
Time2::Time2(int seconds, int minutes, int hours)
{
    this->seconds = seconds;
    this->minutes = minutes;
    this->hours = hours;
}

void Time2::setTime(int seconds, int minutes, int hours)
{
    this->seconds = seconds;
    this->minutes = minutes;
    this->hours = hours;
}

//NUMBER 8
void Time2::outputTime(ostream& out)
{
    out << "Seconds: " << seconds << endl;
    out << "Minutes: " << minutes << endl;
    out << "Hours: " << hours << endl << endl;
    ofstream outfile;
    outfile.open ("data.dat");
    outfile << "Seconds: " << seconds << endl;
    outfile << "Minutes: " << minutes << endl;
    outfile << "Hours: " << hours << endl << endl;
    outfile.close();
}

//NUMBER 10
class Date:Time2
{
private:
    string month;
    int year;
public:
    //NUMBER 11
    Date();
    Date(string, int);
    Date(string, int, int, int, int);
    void outputDate(ostream&);
};

//NUMBER 11 CONT
Date::Date()
:Time2()
{
    month = "";
    year = 0;
}

//NUMBER 11 CONT
Date::Date(string month, int year)
:Time2()
{
    this->month = month;
    this->year = year;
}

//NUMBER 11 CONT
Date::Date(string month, int year, int seconds, int minutes, int hours)
:Time2(seconds, minutes, hours)
{
    this->month = month;
    this->year = year;
}

//NUMBER 12
void Date::outputDate(ostream& out)
{
    out << "Month: " << month << endl;
    out << "Year: " << year << endl;
    this->Time2::outputTime(out);
}

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 3
    cout << "   NUMBER 3" << endl << endl;
    Time now;
    Time1 later = Time1();
    cout << "Seconds: " << now.seconds << endl;
    cout << "Minutes: " << now.minutes << endl;
    cout << "Hours: " << now.hours << endl;
    cout << "Seconds: " << later.seconds << endl;
    cout << "Minutes: " << later.minutes << endl;
    cout << "Hours: " << later.hours << endl << endl;
    
    //NUMBER 4
    cout << "   NUMBER 4" << endl << endl;
    now.seconds = 12;
    now.minutes = 14;
    now.hours = 6;
    cout << "Seconds: " << now.seconds << endl;
    cout << "Minutes: " << now.minutes << endl;
    cout << "Hours: " << now.hours << endl;
    later = Time1(15, 23, 5);
    cout << "Seconds: " << later.seconds << endl;
    cout << "Minutes: " << later.minutes << endl;
    cout << "Hours: " << later.hours << endl << endl;
    
    //NUMBER 5
    //Making the attributes of the Time1 class private makes the data
    //inaccessible through normal means. Instead, I would have to create
    //additional functions within the public part of the class that access and
    //mutate that private data, as well as one function to output the data.
    
    //NUMBER 6
    cout << "   NUMBER 6" << endl << endl;
    Time2 before = Time2();
    before.setTime(5, 26, 12);
    before.outputTime(cout);
    
    //NUMBER 9
    cout << "   NUMBER 9" << endl << endl;
    Time2 sometime = Time2();
    Time2 never = Time2(10, 48, 4);
    sometime.outputTime(cout);
    never.outputTime(cout);
    
    //NUMBER 13
    cout << "   NUMBER 13" << endl << endl;
    Date day1 = Date();
    Date day2 = Date("March",1992);
    Date day3 = Date("April",1989,43,23,2);
    day1.outputDate(cout);
    day2.outputDate(cout);
    day3.outputDate(cout);
    
    return 0;
}

