/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 10/24/2015
* HW: 06
* Problem: ALL (annotated in comments)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>

using namespace std;

/**NUMBER 1 (& NUMBER 3)
 * This function determines which number is largest given three numbers.
 * @param num1 - The first number to be evaluated.
 * @param num2 - The second number to be evaluated.
 * @param num3 - The third number to be evaluated.
 * @return - The largest of the three numbers received.
 */
double max(double num1, double num2, double num3)
{
    if((num1 > num2 && num1 > num3) || (num1 == num2 && num1 == num3))
    {
        return num1;
    }
    else if(num2 > num1 && num2 > num3)
    {
        return num2;
    }
    else if(num3 > num1 && num3 > num2)
    {
        return num3;
    }
}

/**NUMBER 1 (& NUMBER 3)
 * This function determines which number is largest given two numbers.
 * @param num1 - The first number to be evaluated.
 * @param num2 - The second number to be evaluated.
 * @return - The largest of the two numbers received.
 */
double max(double num1, double num2)
{
    if(num1 <= num2)
        return num1;
    else
        return num2;
}

/**NUMBER 2 (& NUMBER 3)
 * This function determines the average serving size for guests eating ice cream
 * @param guests - number of guests to be accounted for
 * @param weight - weight of the ice cream on hand
 * @return - the serving size for each individual guest
 */

double iceCream(int guests, double weight)
{
    double serving = weight/static_cast<double>(guests);
    return serving;
}

/**NUMBER 5
 * This function swaps two integer values.
 * @param num1 - The first integer, to be swapped with the second.
 * @param num2 - The second integer, to be swapped with the first.
 */
void swap(int&num1, int&num2)
{
    int num1a = num1;
    num1 = num2;
    num2 = num1a;
}

/**NUMBER 6 [1]
 * This function converts the weight from pounds/ounces to kilograms/grams.
 * @param pounds - initial pounds value, changed to kilograms value
 * @param ounces - initial ounces value, changed to grams value
 */
void convertWeight(double&pounds, double&ounces)
{
    double tempOunces = ounces / 15;
    double tempPounds = pounds + tempOunces;
    pounds = tempPounds / 2.2046;
    ounces = pounds / 1000;
}
/**NUMBER 6 [2]
 * This function outputs the result of the weight conversion.
 * @param kilograms - weight in kilograms, post-conversion
 * @param grams - weight in gram,s post-conversion
 */
void outputWeight(double kilograms, double grams)
{
    cout << "Total weight is equal to " << kilograms << " kilograms and "
            << grams << " grams." << endl << endl;
}
/**NUMBER 6 [3]
 * This function receives values for the initial weight in pounds and ounces
 * to be converted to kilograms and grams.
 * @param choice - whether or not the player wants to run the program
 */
void inputWeight(int choice)
{
    double num1, num2;
    cout << "TO CONVERT WEIGHT FROM POUNDS/OUNCES TO KILOGRAMS AND GRAMS -";
    cout << endl << "Enter each component of the weight: " << endl
            << "   1. Enter pounds: ";
    cin >> num1;
    cout << "   1. Enter ounces: ";
    cin >> num2;
    convertWeight(num1, num2);
    cout << endl;
    outputWeight(num1, num2);
}

/**NUMBER 7 [1]
 * This function converts length from feet/inches to meters/centimeters
 * @param feet - initial feet value, to be changed to meters value
 * @param inches - initial inches value, to be changed to centimeters value
 */
void convertLength(double&feet, double&inches)
{
    double temp1 = inches / 12;
    double temp2 = feet + temp1;
    feet = temp2 * 0.3048;
    inches = feet * 100.0;      
}

/**NUMBER 7 [2]
 * This function outputs the resulting length in meters and centimeters post-
 * conversion
 * @param meters - post-conversion value for the length in meters
 * @param cmeters - post-conversion value for the length in centimeters
 */
void outputLength(double meters, double cmeters)
{
    cout << "Total length is equal to " << meters << " meters and "
            << cmeters << " centimeters." << endl << endl;
}

/**NUMBER 7 [3]
 * This function begins the length conversion and receives initial values for
 * length in feet and inches from the user
 * @param choice - whether or not user wants to run the program
 */
void inputLength(int choice)
{
    double num1, num2;
    cout << "TO CONVERT LENGTH FROM FEET/INCHES TO METERS AND CENTIMETERS -";
    cout << endl << "Enter each component of the length: " << endl
          << "   1. Enter feet: ";
    cin >> num1;
    cout << "   1. Enter inches: ";
    cin >> num2;
    convertLength(num1, num2);
    cout << endl;
    outputLength(num1, num2);
}

/**NUMBER 8
 * This function computes the number of each particular type of coin needed to
 * make change, and calculates how much change is still leftover afterward
 * @param coin_value - determines type of coin to be evaluated for (25, 10, 1)
 * @param num - number of coins of declared type to be dispensed
 * @param amount_left - remaining coins after evaluation
 */

void compute_coins(int coin_value, int& num, int& amount_left)
{
    num = amount_left / coin_value;
    amount_left = amount_left - num;
}

/**
 * This function receives the total cents needing to be made into change from
 * the user and then outputs how many of each type of coins are needed to make
 * the change.
 * @param choice - whether or not the user wants to run the program
 */
void change(int choice)
{
    int quarters, dimes, pennies;
    int amount_left, num = 0;
    cout << "Enter total amount of change to be dispensed (without decimal): ";
    cin >> amount_left;
    while(!(amount_left < 100 && amount_left > 0))
    {
        cout << "Invalid input." << endl
                << "Enter a new amount: ";
        cin >> amount_left;
    }

    cout << endl << "To make change for " << amount_left << " cents, you need:"
            << endl;
    compute_coins(25, num, amount_left);
    cout << "    - " << num << " quarters" << endl;
    compute_coins(10, num, amount_left);
    cout << "    - " << num << " dimes" << endl;
    compute_coins(1, num, amount_left);
    cout << "    - " << num << " pennies" << endl << endl; 
}

/*
 * 
 */
int main(int argc, char** argv)
{

    //NUMBER 4
    
    int choice;
    cout << "Choose which function to run:" << endl
            << "    1. Find the greatest number" << endl
            << "    2. Calculate average ice cream serving" << endl << endl
            << "Your choice: ";
    cin >> choice;
    cout << endl;    
    switch(choice)
    {
        case 1:
            int choice2;
            double num1, num2, num3;
            cout << "Enter total numbers to be compared (either 2 or 3): ";
            cin >> choice2;
            while(choice2 != 2 && choice2 != 3)
            {
                cout << "Invalid choice. Choose again: ";
                cin >> choice2;
                cout << endl;
            }
            if(choice2 == 2)
            {
                cout << endl << "Enter any two numbers to be compared: ";
                cin >> num1 >> num2;
            }
            else if(choice2 == 3)
            {
                cout << endl << "Enter any three numbers to be compared: ";
                cin >> num1 >> num2 >> num3;
            }
            cout << endl << "The largest number is " << max(num1, num2, num3)
                    << "." << endl << endl;
            break;
        case 2:
            int guests; double weight;
            cout << "Enter the number of guests wanting ice cream: ";
            cin >> guests;
            cout << "Enter the total weight of the ice cream on hand"
                    " (in oz.): ";
            cin >> weight;
            cout << "Each person should get " << iceCream(guests, weight) <<
                    " oz. of ice cream." << endl << endl;
            break;
    }
            
    // NUMBER 6
    
    choice = 0;
    do
    {
        inputWeight(choice);
        cout << "Would you like to convert another weight?" << endl
                << "    1. Yes" << endl
                << "    2. No" << endl << endl
                << "Your choice: ";
        cin >> choice;
    }
    while(choice != 2);
    cout << endl;
    
    // NUMBER 7
    choice = 0;
    do
    {
        inputLength(choice);
        cout << "Would you like to convert another length?" << endl
                << "    1. Yes" << endl
                << "    2. No" << endl << endl
                << "Your choice: ";
        cin >> choice;
    }
    while(choice != 2);
    cout << endl;
    
    // NUMBER 9
    
    choice = 0;
    do
    {
        change(choice);
        cout << "Would you like to calculate more change?" << endl
                << "    1. Yes" << endl
                << "    2. No" << endl << endl
                << "Your choice: ";
        cin >> choice;
    }
    while(choice != 2);
    cout << endl;
    return 0;
}

