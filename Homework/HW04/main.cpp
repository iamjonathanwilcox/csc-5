/*/
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/29/15     
* HW: 04
* Problem: ALL (annotated in comments)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    int num;
    int sum1 = 0, sum2 = 0, sum3 = 0;
    int avg1 = 0, avg2 = 0, x = 0, y = 0;
    for(int i = 0; i < 10 ; i++)
    {
        cout << "Enter a number: ";
        cin >> num;
        if (num > 0)
        {
            sum1 = (sum1 + num);
            avg1 = (avg1 + num);
            x++;
        }
        else
        {
            sum2 = (sum2 + num);
            avg2 = (avg2 + num);
            y++;
        }
            
    }
    
    sum3 = (sum1 + sum2);
    if(x != 0)
    {
        avg1 = (avg1 / x);
    }
    if(y != 0)
    {
        avg2 = (avg2 / y);    
    }
    
           
    cout << "Sum of all positive numbers entered: " << sum1 << endl;
    cout << "Sum of all negative numbers entered: " << sum2 << endl;
    cout << "Average of all positive numbers entered: " << avg1 << endl;
    cout << "Average of all negative numbers entered: " << avg2 << endl;
    cout << "Total sum of all numbers entered: " << sum3 << endl << endl ;
    
    //NUMBER 2
    
    
    sum1 = 0, sum2 = 0, sum3 = 0;
    avg1 = 0, avg2 = 0, x = 0, y = 0;
    for(int i = 0; i < 10 ; i++)
    {
        num = rand()%201-100;
        if (num >= 0)
        {
            sum1 = sum1 + num;
            avg1 = avg1 + num;
            x++;
        }
        else
        {
            sum2 = sum2 + num;
            avg2 = avg2 + num;
            y++;
        }
                
    }
       
    sum3 = (sum1 + sum2);
    if(x != 0)
    {
        avg1 = (avg1 / x);
    }
    if(y != 0)
    {
        avg2 = (avg2 / y);    
    }
    
    cout << "Sum of all positive numbers entered: " << sum1 << endl;
    cout << "Sum of all negative numbers entered: " << sum2 << endl;
    cout << "Average of all positive numbers entered: " << avg1 << endl;
    cout << "Average of all negative numbers entered: " << avg2 << endl;
    cout << "Total sum of all numbers entered: " << sum3 << endl << endl ;
    
    //NUMBER 3
    
    string response;
    cout << "Would you like to know how many candy bars you would have to eat to"
            " maintain your body weight? (Y/N)" << endl;
    cin >> response;
    while(response == "Y" || response == "y")
    {
        
        double weight, bmrF, bmrM, choc;
        int height, age;
        char gender;

        cout << "Are you male or female (M/F)? ";
        cin >> gender;
        cout << "Enter your weight (XX.XX): ";
        cin >> weight;
        cout << "Enter your age: ";
        cin >> age;
        cout << "Enter your height in inches: ";
        cin >> height;

        if(gender == 'M' || gender == 'm')
        {
            bmrM = 66.0 + (6.3 * weight)
                    + (12.9 * static_cast<double>(height))
                    - (6.8 * static_cast<double>(age));        
            choc = (bmrM / 230.00);
        }
        else
        {
            bmrF = 65.0 + (4.3 * static_cast<double>(weight))
                    + (4.7 * static_cast<double>(height))
                    - (4.7 * static_cast<double>(age)); 
            choc = (bmrF / 230.00);
        }

        cout << "To maintain your weight, you would need to eat " << choc
                << " chocolate bars." << endl
                << "Would you like to go again? (Y/N)" << endl;
        cin >> response;
    }
    cout << endl;
    
    //NUMBER 4
    
    sum1 = 0, sum2 = 0; 
    int num1, num2, num3;
    cout << "How many exercises to input? ";
    cin >> num1;
    for(int i = 0; i < num1; i++)
    {
        cout << "Score received for exercise " << (i+1) << ": ";
        cin >> num2;
        cout << "Total points possible for exercise " << (i+1) << ": ";
        cin >> num3;
        sum1 = (sum1 + num2);
        sum2 = (sum2 + num3);
    }
    double avg = (sum1 / static_cast<double>(sum2)) * 100;
    cout << "Your total is " << sum1 << " out of " << sum2 << ", or "
          << fixed << setprecision(2) << avg << "%." << endl << endl;
    
    //NUMBER 5 + NUMBER 6 (just added the outer while loop for number 6)
    
    string choice, choice2;
    cout << "Would you like to play Rock, Paper, Scissors? (Y/N)" << endl;
    cin >> response;
    while(response == "Y" || response == "y")
    {
        
      
        cout << "Player 1, input your choice (R/P/S): ";
        cin >> choice;
        cout << "Player 2, input your choice (R/P/S): ";
        cin >> choice2;

        while(choice != "R" && choice != "r" && choice != "P" && choice != "p"
                && choice != "S" && choice != "s")
        {
            cout << "PLAYER 1: "
                    "That is not a valid input. Please choose either R, P, or S: ";
            cin >> choice;
        }
        while(choice2 != "R" && choice2 != "r" && choice2 != "P" && choice2 != "p"
                && choice2 != "S" && choice2 != "s")
        {
            cout << "PLAYER 2: "
                    "That is not a valid input. Please choose either R, P, or S: ";
            cin >> choice2;
        }

        if(choice == "R" || choice == "r")
        {
            if(choice2 == "R" || choice2 == "r")
            {
                cout << "Nobody wins." << endl;
            }
            else if(choice2 == "P" || choice2 == "p")
            {
                cout << "Paper beats rock. Player 1 wins!" << endl;
            }
            else if(choice2 == "S" || choice2 == "s")
            {
                cout << "Rock beats scissors. Player 2 wins!" << endl;
            }

        }
        else if(choice == "P" || choice == "p")
        {
            if(choice2 == "R" || choice2 == "r")
            {
                cout << "Paper beats rock. Player 1 wins!" << endl;
            }
            else if(choice2 == "P" || choice2 == "p")
            {
                cout << "Nobody wins." << endl;
            }
            else if(choice2 == "S" || choice2 == "s")
            {
                cout << "Scissors beats paper. Player 2 wins!" << endl;
            }
        }
        else if(choice == "S" || choice == "s")
        {
            if(choice2 == "R" || choice2 == "r")
            {
                cout << "Rock beats scissors. Player 2 wins!" << endl;
            }
            else if(choice2 == "P" || choice2 == "p")
            {
                cout << "Scissors beats paper. Player 1 wins!" << endl;
            }
            else if(choice2 == "S" || choice2 == "s")
            {
                cout << "Nobody wins." << endl;
            }
        }
        cout << "Would you like to play again? (Y/N)" << endl;
        cin >> response;
    }
    return 0;
}

