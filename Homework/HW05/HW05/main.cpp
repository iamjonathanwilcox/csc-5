/*/
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 10/10/15    
* HW: 05
* Problem: ALL (annotated in comments, all non-program problems are saved in
 * the HW05 folder)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

//==========================================================================
//NUMBER 4
//==========================================================================
void UserRockPaper(string choice, string choice2)
{
    while(choice != "R" && choice != "r" && choice != "P" && choice != "p"
                    && choice != "S" && choice != "s")
        {
            cout << "PLAYER 1: "
                    "That is not a valid input. Please choose either R, P, or S: ";
            cin >> choice;
        }
        while(choice2 != "R" && choice2 != "r" && choice2 != "P" && choice2 != "p"
                && choice2 != "S" && choice2 != "s")
        {
            cout << "PLAYER 2: "
                    "That is not a valid input. Please choose either R, P, or S: ";
            cin >> choice2;
        }

        if(choice == "R" || choice == "r")
        {
            if(choice2 == "R" || choice2 == "r")
            {
                cout << "Nobody wins." << endl;
            }
            else if(choice2 == "P" || choice2 == "p")
            {
                cout << "Paper beats rock. Player 1 wins!" << endl;
            }
            else if(choice2 == "S" || choice2 == "s")
            {
                cout << "Rock beats scissors. Player 2 wins!" << endl;
            }

        }
        else if(choice == "P" || choice == "p")
        {
            if(choice2 == "R" || choice2 == "r")
            {
                cout << "Paper beats rock. Player 1 wins!" << endl;
            }
            else if(choice2 == "P" || choice2 == "p")
            {
                cout << "Nobody wins." << endl;
            }
            else if(choice2 == "S" || choice2 == "s")
            {
                cout << "Scissors beats paper. Player 2 wins!" << endl;
            }
        }
        else if(choice == "S" || choice == "s")
        {
            if(choice2 == "R" || choice2 == "r")
            {
                cout << "Rock beats scissors. Player 2 wins!" << endl;
            }
            else if(choice2 == "P" || choice2 == "p")
            {
                cout << "Scissors beats paper. Player 1 wins!" << endl;
            }
            else if(choice2 == "S" || choice2 == "s")
            {
                cout << "Nobody wins." << endl;
            }
        }
}

void AIRockPaper(int cpuChoice, int cpuChoice2)
{
    if(cpuChoice = 0)
    {
        cout << " - CPU 1 CHOICE: Rock" << endl;
        if(cpuChoice2 = 0)
        {
            cout << " - CPU 2 CHOICE: Rock" << endl;
            cout << "Nobody wins." << endl;
        }
        else if(cpuChoice2 = 1)
        {
            cout << " - CPU 2 CHOICE: Paper" << endl;
            cout << "Paper beats rock. CPU 1 wins!" << endl;
        }
        else
        {
            cout << " - CPU 2 CHOICE: Scissors" << endl;
            cout << "Rock beats scissors. CPU 2 wins!" << endl;
        }

    }
    else if(cpuChoice = 1)
    {
        cout << " - CPU 1 CHOICE: Paper" << endl;
        if(cpuChoice2 = 0)
        {
            cout << " - CPU 2 CHOICE: Rock" << endl;
            cout << "Paper beats rock. CPU 1 wins!" << endl;
        }
        else if(cpuChoice2 = 1)
        {
            cout << " - CPU 2 CHOICE: Paper" << endl;
            cout << "Nobody wins." << endl;
        }
        else
        {
            cout << " - CPU 2 CHOICE: Scissors" << endl;
            cout << "Scissors beats paper. CPU 2 wins!" << endl;
        }
    }
    else
    {
        if(cpuChoice2 = 0)
        {
            cout << " - CPU 2 CHOICE: Rock" << endl;
            cout << "Rock beats scissors. CPU 2 wins!" << endl;
        }
        else if(cpuChoice = 1)
        {
            cout << " - CPU 2 CHOICE: Paper" << endl;
            cout << "Scissors beats paper. CPU 1 wins!" << endl;
        }
        else
        {
            cout << " - CPU 2 CHOICE: Scissors" << endl;
            cout << "Nobody wins." << endl;
        }
    }
}
/*
 * 
 */
int main(int argc, char** argv)
{
    //==========================================================================
    //NUMBER 3
    //==========================================================================
    
    string response, choice, choice2;
    cout << "Would you like to play Rock, Paper, Scissors? (Y/N)" << endl;
    cin >> response;
    while(response == "Y" || response == "y")
    {
        int version = 0;
        do
        { 
            cout << "Which version do you want to play?" << endl
                    << "    1. 2-Player Version" << endl
                    << "    2. CPU Version" << endl
                    << "Type 1 or 2 to choose: ";
            cin >> version;
            if(version != 1 && version != 2)
            {
                cout << "That is not a valid choice." << endl;
            }
        }
        while(version != 1 && version != 2);
        
        
        if(version = 1 && version != 2)
        {   
            cout << "Player 1, input your first choice (R/P/S): ";
            cin >> choice;
            cout << "Player 2, input your first choice (R/P/S): ";
            cin >> choice2;
            UserRockPaper(choice, choice2);
        }
        if(version = 2 && version != 1)
        {
            int cpuChoice = rand()%3;
            int cpuChoice2 = rand()%3;
            AIRockPaper(cpuChoice, cpuChoice2);
        }

        
        
        cout << "Would you like to play again? (Y/N)" << endl;
        cin >> response;
    }
    cout << endl;
    
    
    //==========================================================================
    //NUMBER 5
    //==========================================================================
    
    cout << "Do you wish to know your average miles-per-gallon for your"
            " vehicle? (Y/N)" << endl;
    cin >> response;
    while(response == "Y" || response == "y")
    {
        double liters, gallons, miles, mpg;
        cout << "Enter the number of miles driven: ";
        cin >> miles;
        cout << "Enter the amount of gasoline consumed by your car in liters: ";
        cin >> liters;
        
        gallons = 0.264179 * liters;
        mpg = miles / gallons;
        
        cout << "You are currently averaging " << mpg << " miles-per-gallon."
                << endl
                << "Would you like to calculate miles-per-gallon again?"
                << endl;
        cin >> response;
    }
    cout << endl;
    
    //==========================================================================
    //NUMBER 7
    //==========================================================================
    
    cout << "Do you wish to know your average miles-per-gallon for your"
            " vehicle by loading in data from an external file? (Y/N)" << endl;
    cin >> response;
    while(response == "Y" || response == "y")
    {
        
        double liters, gallons, miles, mpg;
        ifstream inFile;
        inFile.open("data.dat");
        cout << "Reading data from 'data.dat'..." << endl;
        
        inFile >> miles >> liters;
        
        gallons = 0.264179 * liters;
        mpg = miles / gallons;
        
        inFile.close();
        
        cout << "You are currently averaging " << mpg << " miles-per-gallon."
                << endl
                << "Would you like to calculate miles-per-gallon again?"
                << endl;
        cin >> response;
    }
    cout << endl;
    
    //==========================================================================
    //NUMBER 9
    //==========================================================================
    
    cout << "Comparing the miles-per-gallon of two cars using external data."
            << endl << "Reading data from data2.dat..." << endl;
    
    double liters1, gallons1, miles1, mpg1, liters2, gallons2, miles2, mpg2;
    ifstream inFile;
    inFile.open("data2.dat");

    inFile >> miles1 >> liters1 >> miles2 >> liters2;

    gallons1 = 0.264179 * liters1;
    mpg1 = miles1 / gallons1;
    
    gallons2 = 0.264179 * liters2;
    mpg2 = miles2 / gallons2;
    
    inFile.close();
    
    cout << "Vehicle 1 averages " << mpg1 << " miles-per-gallon." << endl
            << "Vehicle 2's averages " << mpg2 << " miles-per-gallon" << endl;
    if(mpg1 > mpg2)
    {
        cout << "Vehicle 1 has the superior miles-per-gallon ratio." << endl;
    }
    else if(mpg1 < mpg2)
    {
        cout << "Vehicle 2 has the superior miles-per-gallon ratio." << endl;
    }
    else
    {
        cout << "Vehicle 1 and Vehicle 2 both share the same miles-per-gallon "
                "ratio.";
    }
    
    
    
    return 0;
}

