RockPaperScissors()
	Initialize response, Player 1 Choice, Player 2 Choice
	OUTPUT Would you like to play R/P/S?
	INPUT response
	WHILE response = "y" or "Y"     
	        INPUT Player 1 Choice
		INPUT Player 2 Choice
        	WHILE Player 1 Choice != R, P, or S
        		OUTPUT Not a valid choice.
                	INPUT Player 1 Choice
        	END WHILE
       	 	WHILE Player 2 Choice != R, P, or S
        		OUTPUT Not a valid choice.
			INPUT Player 2 Choice
        	END WHILE
        	IF Player 1 Choice = "R" or "r"
            		IF Player 2 Choice = "R" or "r"
                		OUTPUT Nobody wins
            		ELSE IF Player 2 Choice = "P" or "p"
                		OUTPUT Player 2 wins
            		ELSE IF Player 2 Choice = "S" or "s"
            			OUTPUT Player 1 wins
			END IF
		END IF
        	IF Player 1 Choice = "P" or "p"
            		IF Player 2 Choice = "R" or "r"
                		OUTPUT Player 1 wins
            		ELSE IF Player 2 Choice = "P" or "p"
                		OUTPUT Nobody wins
            		ELSE IF Player 2 Choice = "S" or "s"
            			OUTPUT Player 2 wins
			END IF
		END IF
        	IF Player 1 Choice = "S" or "s"
            		IF Player 2 Choice = "R" or "r"
                		OUTPUT Player 2 wins
            		ELSE IF Player 2 Choice = "P" or "p"
                		OUTPUT Player 1 wins
            		ELSE IF Player 2 Choice = "S" or "s"
            			OUTPUT Nobody wins
			END IF       
		END IF
		OUTPUT Would you like to play again
		INPUT response
	END WHILE