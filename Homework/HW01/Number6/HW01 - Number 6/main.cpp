/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/8/2015
* HW: Homework 7
* Problem: Annotated by numbers comments within code - 
 * 1) Copy program text from book
 * 2) Add beginning "hello" and ending "good-bye" lines
 * 3) Replace the * in the initial program with /, test.
 * 4) Replace the * in the initial program with +, test.
 * 5) Write a program that receives two integers and then calculates their sum.
 * 6) Experiment with the effects deliberate errors will have on the executability of a program.
 * 7) Output stylized CS! message with the tagline "Computer Science sure is cool stuff!"
* I certify this is my own work and code
*/


#include <iostream>
using namespace std;

/*
 * 
 */
int main( ) {

    //Number 6
    
    (int) number_of_pods, peas_per_pod, total_peas;
    
    cout << "Hello!\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout >> "Enter the number of peas in a pod:\n";  
    cin >> peas_per_pod;
    total_peas = number_of_pods * peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << "peas_per_pod";
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n"
    cout << "Good-bye!\n";
    
    return 0;
}

