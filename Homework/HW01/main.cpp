/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/8/2015
* HW: Homework 7
* Problem: Annotated by numbers comments within code - 
 * 1) Copy program text from book
 * 2) Add beginning "hello" and ending "good-bye" lines
 * 3) Replace the * in the initial program with /, test.
 * 4) Replace the * in the initial program with +, test.
 * 5) Write a program that receives two integers and then calculates their sum.
 * 6) Experiment with the effects deliberate errors will have on the executability of a program.
 * 7) Output stylized CS! message with the tagline "Computer Science sure is cool stuff!"
* I certify this is my own work and code
*/


#include <iostream>
using namespace std;

/*
 * 
 */
int main( ) {

    //Number 1 + Number 2 (included together because number 2 is only a minor addition to number 1 with no other changes)
    
    int number_of_pods, peas_per_pod, total_peas;
    
    cout << "Hello!\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";  
    cin >> peas_per_pod;
    total_peas = number_of_pods * peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n";
    cout << "Good-bye!\n";
    
    //Number 3
    int number_of_pods2, peas_per_pod2, total_peas2;
    
    cout << "Hello!\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods2;
    
    cout << "Enter the number of peas in a pod:\n";  
    cin >> peas_per_pod2;
    total_peas2 = number_of_pods2 * peas_per_pod2;
    cout << "If you have ";
    cout << number_of_pods2;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod2;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas2;
    cout << " peas in all the pods.\n";
    cout << "Good-bye!\n";
    
    //Number 4
    cout << "\n====================\n\n";
    cout << "Hello!\n";
    cout << "Press return after entering a number.\n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";  
    cin >> peas_per_pod;
    total_peas = number_of_pods + peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n";
    cout << "Good-bye!\n";
    //Changing the * to a + causes an error in the calculation of total_peas.
    
    //Number 5
    int partOne, partTwo, sum;
    cout << "\n====================\n\n";
    cout << "Enter a whole number: ";
    cin >> partOne;
    cout << "Enter another whole number: ";
    cin >> partTwo;
    sum = partTwo + partOne;
    cout << "The sum of these two numbers is " << sum << ".\n";
    
    //Number 6 was written in a separate program because it deliberately causes errors that cause the program to be unable to run.
    //See folder "csc-5/Homework/HW01/Number 6".
    
    //Number 7
    cout << "\n====================\n\n";
    cout << "********************************************************************************" << endl << endl;
    cout << "                 C  C  C                S  S  S  S             !!       " << endl;
    cout << "               C         C            S            S           !!       " << endl;
    cout << "             C                      S                S         !!       " << endl;
    cout << "             C                        S                        !!       " << endl;
    cout << "             C                          S                      !!       " << endl;
    cout << "             C                             S  S  S             !!       " << endl;
    cout << "             C                                     S           !!       " << endl;
    cout << "             C                                       S         !!       " << endl;
    cout << "             C                                         S       !!       " << endl;
    cout << "             C                         S             S         !!       " << endl;
    cout << "               C         C               S         S                    " << endl;
    cout << "                 C  C  C                   S  S  S             00       " << endl << endl;
    cout << "********************************************************************************" << endl;
    cout << "                  Computer Science is cool stuff!                       " << endl;
    
    
    return 0;
   
}

