/*/
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 12/06/2015  
* HW: 09
* Problem: ALL (annotated in comments and output)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>

using namespace std;

/**
 * This function creates a dynamic array of a pre-defined size
 * @param size - size of array to be created
 * @return - integer pointer to location of dynamic array
 */
int* createDA(int size)
{
    int* p = new int[size];
    return p;
}

/**
 * This function inserts random values into a dynamic array
 * @param p - pointer of dynamic array location
 * @param size - size of dynamic array
 */
void insertVal(int*&p, int size)
{
    srand(time(0));
    for(int i = 0; i < size; i++)
        p[i] = rand()%100;
}

/**
 * This function outputs the values within a dynamic array
 * @param p - pointer of dynamic array location 
 * @param size - size of dynamic array
 */
void outputDA(int* p, int size)
{
    cout << "Array: ";
    for(int i = 0; i < size; i++)
        cout << p[i] << " ";
}

/**
 * This function outputs the values within a dynamic array of strings
 * @param s - pointer of dynamic array location
 * @param size - size of dynamic array
 */
void outputDA(string* s, int size)
{
    cout << "Array: " << endl;
    for(int i = 0; i < size; i++)
        cout << "   " << s[i] << "\n";
}

/**
 * This function adds a new value to a dynamic array by replacing it with a new
 * dynamic array with an increased size
 * @param p - pointer of dynamic array location
 * @param size - size of dynamic array
 * @param val - value to be added to array
 */
void addVal(int*&p, int&size, int val)
{
    int* temp = new int[size+1];
    for(int i = 0; i < size; i++)
        temp[i] = p[i];
    delete[] p;
    p = temp;
    p[size] = val;
    size++;
}

/**
 * This function adds a specified value to a dynamic array of strings
 * @param s - pointer of dynamic array location
 * @param size - size of dynamic array
 * @param val - value to be added to dynamic array
 */
void addVal(string*&s, int&size, string val)
{
    string* temp = new string[size+1];
    for(int i = 0; i < size; i++)
        temp[i] = s[i];
    delete[] s;
    s = temp;
    s[size] = val;
    size++;
}

/**
 * This function adds a specified value to a dynamic array of strings at a
 * specific location
 * @param s - pointer of dynamic array location
 * @param size - size of dynamic array
 * @param val - value to be inserted
 * @param loc - location to insert value
 */
void addVal(string*&s, int&size, string val, int loc)
{
    string* temp = new string[size+1];
    for(int i = 0; i < loc; i++)
        temp[i] = s[i];
    temp[loc] = val;
    for(int i = loc+1; i < size+1; i++)
        temp[i] = s[i-1];
    delete[] s;
    s = temp;
    size++;  
}

/**
 * This function deletes a value from a dynamic array of strings
 * @param s - pointer of dynamic array location
 * @param size - size of dynamic array
 * @param loc - location of value to be deleted
 */
void deleteEntry(string*&s, int& size, int loc)
{
    string* temp = new string[size-1];
    int counter = 0;
    for(int i = 0; i < loc; i++)
    {
        temp[i] = s[i];
        counter++;
    }
    for(int i = loc+1; i < size; i++)
    {
        temp[counter] = s[i];
        counter++;
    }
    delete[] s;
    s = temp;
    size--;
}

/**
 * This function deletes a value specified by the user from a dynamic array of
 * strings
 * @param s - pointer of dynamic array location
 * @param size - size of dynamic array
 * @param input - string value user wants to remove
 */
void deleteEntry(string*&s, int& size, string input)
{
    int loc = -1;
    string* temp = new string[size-1];
    int counter = 0;
    for(int i = 0; i < size; i++)
    {
        if(s[i] == input)
        {
            loc = i;
        }
    }
    if(loc == -1) return;
    for(int i = 0; i < loc; i++)
    {
        temp[i] = s[i];
        counter++;
    }
    for(int i = loc+1; i < size; i++)
    {
        temp[counter] = s[i];
        counter++;
    }
    delete[] s;
    s = temp;
    size--;
}

/**
 * This function counts how many times each character appears within a given
 * string
 * @param input - string to be evaluated
 * @return - pointer to dynamic array of character count values
 */
int* charCount(string input)
{
    int* counter = new int[26];
    for(int i = 0; i  < 25; i++)
    {
        counter[i] = 0;
    }
    for(int i = 0; i < input.size(); i++)
    {
        if(tolower(input[i]) == 'a')
            counter[0]++;
        if(tolower(input[i]) == 'b')
            counter[1]++;
        if(tolower(input[i]) == 'c')
            counter[2]++;
        if(tolower(input[i]) == 'd')
            counter[3]++;
        if(tolower(input[i]) == 'e')
            counter[4]++;
        if(tolower(input[i]) == 'f')
            counter[5]++;
        if(tolower(input[i]) == 'g')
            counter[6]++;
        if(tolower(input[i]) == 'h')
            counter[7]++;
        if(tolower(input[i]) == 'i')
            counter[8]++;
        if(tolower(input[i]) == 'j')
            counter[9]++;
        if(tolower(input[i]) == 'k')
            counter[10]++;
        if(tolower(input[i]) == 'l')
            counter[11]++;
        if(tolower(input[i]) == 'm')
            counter[12]++;
        if(tolower(input[i]) == 'n')
            counter[13]++;
        if(tolower(input[i]) == 'o')
            counter[14]++;
        if(tolower(input[i]) == 'p')
            counter[15]++;
        if(tolower(input[i]) == 'q')
            counter[16]++;
        if(tolower(input[i]) == 'r')
            counter[17]++;
        if(tolower(input[i]) == 's')
            counter[18]++;
        if(tolower(input[i]) == 't')
            counter[19]++;
        if(tolower(input[i]) == 'u')
            counter[20]++;
        if(tolower(input[i]) == 'v')
            counter[21]++;
        if(tolower(input[i]) == 'w')
            counter[22]++;
        if(tolower(input[i]) == 'x')
            counter[23]++;
        if(tolower(input[i]) == 'y')
            counter[24]++;
        if(tolower(input[i]) == 'z')
            counter[25]++;
    }
    return counter;
}

/**
 * This function creates a dynamic array of characters representing the alphabet
 * @return - pointer to dynamic array location of alphabet characters
 */
char* alphabetDA()
{
    char* a = new char[26];
    a[0] = 'a'; a[1] = 'b'; a[2] = 'c'; a[3] = 'd'; a[4] = 'e'; a[5] = 'f'; 
    a[6] = 'g'; a[7] = 'h'; a[8] = 'i'; a[9] = 'j'; a[10] = 'k'; a[11] = 'l'; 
    a[12] = 'm'; a[13] = 'n'; a[14] = 'o'; a[15] = 'p'; a[16] = 'q';
    a[17] = 'r'; a[18] = 's'; a[19] = 't'; a[20] = 'u'; a[21] = 'v';
    a[22] = 'w'; a[23] = 'x'; a[24] = 'y'; a[25] = 'z'; 
    return a;
}
/*
 * 
 */
int main(int argc, char** argv) {

    int size;
    
    //NUMBER 1 AND 2
    cout << "   NUMBER 1 (+ NUMBER 2)" << endl << endl;
    cout << "Enter size of array: ";
    cin >> size;
    int* p = createDA(size);
    insertVal(p, size);
    outputDA(p, size);
    delete[] p;
    cout << endl << endl;
    
    //NUMBER 3
    cout << "   NUMBER 3" << endl << endl;
    int val;
    size = 0;
    int* q = createDA(size);
    do
    {
        cout << "Enter number to add to array (enter -999 to stop): ";
        cin >> val;
        if(val != -999)
            addVal(q, size, val);
    }
    while(val != -999);
    outputDA(q, size);
    delete[] q;
    cout << endl << endl;
    
    //NUMBER 4
    cout << "   NUMBER 4" << endl << endl;
    size = 5;
    int* r = createDA(size);
    int counter = 0;
    do
    {
        cout << "Enter number to add to array (enter -999 to stop): ";
        cin >> val;
        if(val != -999 && counter < 5)
            r[counter] = val;
        counter++;
    }
    while(val != -999);
    outputDA(r, size);
    delete[] r;
    cout << endl << endl;
    
    //NUMBER 5
    int loc = 2;
    cout << "   NUMBER 5" << endl << endl;
    size = 5;
    string* s = new string[size];
    s[0] = "Franklin";
    s[1] = "Gertrude";
    s[2] = "Bartholomew" ;
    s[3] = "Narcissa";
    s[4] = "Smedley";
    outputDA(s, size);
    deleteEntry(s, size, loc);
    outputDA(s, size);
    delete[] s;
    cout << endl << endl;
    
    //NUMBER 6
    cout << "   NUMBER 6" << endl << endl;
    size = 5;
    string newVal = "Esposito";
    string* t = new string[size];
    t[0] = "Fernanda";
    t[1] = "Grimwald";
    t[2] = "Brumhilda";
    t[3] = "Nathaniel";
    t[4] = "Striga";
    outputDA(t, size);
    addVal(t, size, newVal);
    outputDA(t, size);
    delete[] t;
    cout << endl << endl;
    
    //NUMBER 7
    cout << "   NUMBER 7" << endl << endl;
    size = 5; int newLoc;
    newVal = "Esmerelda";
    string* u = new string[size];
    u[0] = "Franz";
    u[1] = "Giana";
    u[2] = "Buster";
    u[3] = "Natalia";
    u[4] = "Sirius";
    outputDA(u, size);
    cout << "Enter location desired for additional name: ";
    cin >> newLoc;
    addVal(u, size, newVal, newLoc);
    outputDA(u, size);
    delete[] u;
    cout << endl << endl;
    
    //NUMBER 8
    cout << "   NUMBER 8" << endl << endl;
    size = 5;
    string input = "";
    string* v = new string[size];
    v[0] = "Flamilda";
    v[1] = "Gordo";
    v[2] = "Beatrice" ;
    v[3] = "Nedston";
    v[4] = "Shawnda";
    outputDA(v, size);
    cout << "Enter name you would like to delete: ";
    cin >> input;
    deleteEntry(v, size, input);
    outputDA(v, size);
    delete[] v;
    cout << endl << endl;
    
    //NUMBER 10
    cout << "   NUMBER 10" << endl << endl
         << "Enter line of text to be evaluated: ";
    getline(cin >> ws, input); //Found this online, wasn't correctly reading
    int* w = charCount(input);  //input from user with getline before adding the
    char* abc = alphabetDA();   // '>> ws' component
    cout << "Letter | Number of Occurrences" << endl;
    for(int i = 0; i < 25; i++)
    {
        if(w[i] != 0)
            cout << abc[i] << "           " << w[i] << endl;
    }
    cout << endl << endl;
    
    //NUMBER 11
    cout << "   NUMBER 11" << endl << endl;
    int num1 = 42;
    int num2 = num1;
    int* p1 = &num1;
    int* p2 = &num2;
    cout << "num1: " << num1 << endl << "num2: " << num2 << endl;
    num1 = 12;
    cout << "num1: " << num1 << endl << "num2: " << num2 << endl;
    *p1 = 25;
    cout << "num1: " << num1 << endl << "num2: " << num2 << endl;
    *p1 = *p2; //Is this what you meant for us to do here? I'm semi-confused by
    *p2 = 60;   //what the question is asking us to do, but I think this is ok.
    cout << "num1: " << num1 << endl << "num2: " << num2 << endl;
    return 0;
}

