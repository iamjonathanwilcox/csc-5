/*
* Name: Jonathan Wilcox
* Student ID: 2569288
* Date: 9/24/15
* HW: 03
* Problem: ALL (annotated in comments)
* I certify this is my own work and code
*/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstring>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //NUMBER 1
    
    int num1;
    string phoneNum, num2, num3;
    cout << "Enter a telephone number: ";
    cin >> phoneNum;
    num1 = phoneNum.find("-");
    num2 = phoneNum.substr(0, num1-1);
    num3 = phoneNum.substr(num1+1);
    cout << num2 << num3 << endl << endl;
    
    //NUMBER 2
    
    int apples, oranges, pears;
    cout << "Enter the number of apples: ";
    cin >> apples;
    cout << "Enter the number of oranges: ";
    cin >> oranges;
    cout << "Enter the number of pears: ";
    cin >> pears;
    
    int x = 0; //apples
    int y = 0; //oranges
    int z = 0; //pears
    
    if(apples < oranges && apples < pears)
    {
        while(apples < oranges)
        {
            oranges--;
            y++;
        }
        while(apples < pears)
        {
            pears--;
            z++;
        }
        cout << "You should leave " << x << " apples." << endl
             << "You should leave " << y << " oranges." << endl
             << "You should leave " << z << " pears.";
    }
    else if(oranges < apples && oranges < pears)
    {
        while(oranges < apples )
        {
            apples--;
            x++;
        }
        while(oranges < pears)
        {
            pears--;
            z++;
        }
        cout << "You should leave " << x << " apples." << endl
             << "You should leave " << y << " oranges." << endl
             << "You should leave " << z << " pears.";  
    }
    else if(pears <= apples && pears <= oranges)
    {
        while(pears < apples)
        {
            apples--;
            x++;
        }
        while(pears < oranges)
        {
            oranges--;
            y++;
        }
        cout << "You should leave " << x << " apples." << endl
             << "You should leave " << y << " oranges." << endl
             << "You should leave " << z << " pears.";
    }
    else
    {
        cout << "SOMETHING WENT WRONG!";
    }
    cout << endl << endl;
    
    //NUMBER 3
    
    int maxCap, current;
    cout << "Enter the max capacity of people in room: ";
    cin >> maxCap; 
    cout << "Enter the current number of people in room: ";
    cin >> current;
    
    if(current <= maxCap)
    {
        cout << "You are currently NOT in violation of fire code.";
    }
    else
    {
        cout << "You are currently in violation of fire code.";
    }
    cout << endl << endl;
    
    //NUMBER 4
    
    int num;
    int sum1 = 0, sum2 = 0, sum3 = 0;
    for(int i = 0; i < 10 ; i++)
    {
        cout << "Enter a number: ";
        cin >> num;
        if (num >= 0)
        {
            sum1 = sum1 + num;
        }
        else
        {
            sum2 = sum2 + num;
        }
        sum3 = sum1 + sum2;
        
    }
    cout << "Sum of all positive numbers entered: " << sum1 << endl;
    cout << "Sum of all negative numbers entered: " << sum2 << endl;
    cout << "Total sum of all numbers entered: " << sum3 << endl << endl ;
    
    return 0;
}

