/* 
 * File:   main.cpp
 * Author: Jonathan
 *
 * Created on December 13, 2015, 10:48 PM
 */

#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

void ticTacToe();
void codeBreaker();

class Board
{
private:
    char** board; //The board
    char p1; //The character seen on the board for player's choice
    char p2; //The character seen on the board for player's choice
public:
    Board();
    void player1Place(int);
    void player2Place(int);
    bool checkChoice(int);
    int dumbAIChoice();
    int smartAIChoice();
    void checkBoard(string&);
    void outputBoard();
    int minimax(int);
};

void player_vPlayer();
void player_vDumbAI();
void player_vSmartAI();
void dumbAI_vDumbAI();


class Code
{
private:
    int size;
    int turn;
    vector<int> code;
    vector<int> guess;
    vector<char> progress;
public:
    Code();
    void setSize(int);
    void makeCode();
    void makeGuess(int);
    void makeProgress();
    bool winStatus();
    void outputCode();
    void outputGuess();
    void outputProgress();
    void resetGuess();
    void incTurn();
    int getTurn();
};

void playGame(int size);

int main(int argc, char** argv) {
    
    int choice;
    cout << "CHOOSE A GAME:" << endl
            << "    1. Tic Tac Toe" << endl
            << "    2. Codebreaker" << endl << endl
            << "Your choice: ";
    cin >> choice;
    switch(choice)
    {
        case 1:
            ticTacToe();
            break;
        case 2:
            codeBreaker();
            break;
        default:
            cout << "Invalid Input!";
    }
    cout << endl;

    return 0;
}

/**
 * This function runs the game Tic Tac Toe
 */
void ticTacToe()
{
    int choice;
    cout << "TIC TAC TOE!" << endl
            << "    1. Player vs. Player" << endl
            << "    2. Player vs. Dumb AI" << endl
            << "    3. Player vs. Smart AI" << endl
            << "    4. Dumb AI vs. Dumb AI" << endl
            << "Your choice: ";
    cin >> choice;
    cout << endl;
    switch(choice)
    {
        case 1:
            player_vPlayer();
            break;
        case 2:
            player_vDumbAI();
            break;
        case 3:
            player_vSmartAI();
            break;
        case 4:
            dumbAI_vDumbAI();
            break;
        default:
            cout << "Invalid Input!";
    }
}

/**
 * This function runs the game Codebreaker
 */
void codeBreaker()
{
    cout << "   CODEBREAKER!" << endl << endl;
    
    cout << "The computer will generate a code and it is up to you to decipher"
            << " it!\nInput your guess, and the computer will tell you:" << endl
            << "     - Which numbers are correct [!]" << endl
            << "     - Which numbers are in the code, but in a different spot"
            << " [?]" << endl
            << "     - Which numbers are incorrect [X]" << endl << endl;
    
    int choice;
    cout << "Choose your difficulty:" << endl
            << "    1. Easy" << endl
            << "    2. Medium" << endl
            << "    3. Hard" << endl << endl
            << "Your choice: ";
    cin >> choice;
    int size = 0;
    switch(choice)
    {
        case 1:
            size = 4;
            playGame(size);
            break;
        case 2:
            size = 5;
            playGame(size);
            break;
        case 3:
            size = 6;
            playGame(size);
            break;
        default:
            cout << "Invalid input!";
    } 
    cout << endl;
}

/**
 * Board constructor
 */
Board::Board()
{
    board = new char*[3];
    for(int i = 0; i < 3; i++)
        board[i] = new char[3];
        
    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            board[i][j] = ' ';
        }
    }  
    p1 = 'X';
    p2 = 'O';
}

/**
 * This function places player 1's mark on the board
 * @param choice - the location to place player 1's mark
 */
void Board::player1Place(int choice)
{
    if(choice == 1 && board[0][0] == ' ')
        board[0][0] = p1;
    if(choice == 2 && board[0][1] == ' ')
        board[0][1] = p1;
    if(choice == 3 && board[0][2] == ' ')
        board[0][2] = p1;
    if(choice == 4 && board[1][0] == ' ')
        board[1][0] = p1;
    if(choice == 5 && board[1][1] == ' ')
        board[1][1] = p1;
    if(choice == 6 && board[1][2] == ' ')
        board[1][2] = p1;
    if(choice == 7 && board[2][0] == ' ')
        board[2][0] = p1;
    if(choice == 8 && board[2][1] == ' ')
        board[2][1] = p1;
    if(choice == 9 && board[2][2] == ' ')
        board[2][2] = p1;
}

/**
 * This function places player 2's mark on the board;
 * @param choice - location to place player 2's mark
 */
void Board::player2Place(int choice)
{
    if(choice == 1 && board[0][0] == ' ')
        board[0][0] = p2;
    if(choice == 2 && board[0][1] == ' ')
        board[0][1] = p2;
    if(choice == 3 && board[0][2] == ' ')
        board[0][2] = p2;
    if(choice == 4 && board[1][0] == ' ')
        board[1][0] = p2;
    if(choice == 5 && board[1][1] == ' ')
        board[1][1] = p2;
    if(choice == 6 && board[1][2] == ' ')
        board[1][2] = p2;
    if(choice == 7 && board[2][0] == ' ')
        board[2][0] = p2;
    if(choice == 8 && board[2][1] == ' ')
        board[2][1] = p2;
    if(choice == 9 && board[2][2] == ' ')
        board[2][2] = p2;
}

/**
 * This function checks the board for a winner
 * @param winner - who the winner of the current game is, if there is one
 */
void Board::checkBoard(string& winner)
{
    if(board[0][0] == board[0][1] && board[0][0] == board[0][2])
    {
        if(board[0][0] == 'X') winner = "p1";
        else if(board[0][0] == 'Y') winner = "p2";
    }
    if(board[1][0] == board[1][1] && board[1][0] == board[1][2])
    {
        if(board[1][0] == 'X') winner = "p1";
        else if(board[1][0] == 'Y') winner = "p2";
    }
    if(board[2][0] == board[2][1] && board[2][0] == board[2][2])
    {
        if(board[2][0] == 'X') winner = "p1";
        else if(board[2][0] == 'Y') winner = "p2";
    }
    if(board[0][0] == board[1][0] && board[0][0] == board[2][0])
    {
        if(board[0][0] == 'X') winner = "p1";
        else if(board[0][0] == 'Y') winner = "p2";
    }
    if(board[0][1] == board[1][1] && board[0][1] == board[2][1])
    {
        if(board[0][1] == 'X') winner = "p1";
        else if(board[0][1] == 'Y') winner = "p2";
    }
    if(board[0][2] == board[1][2] && board[0][2] == board[2][2])
    {
        if(board[0][2] == 'X') winner = "p1";
        else if(board[0][2] == 'Y') winner = "p2";
    }
    if(board[0][0] == board[1][1] && board[0][0] == board[2][2])
    {
        if(board[0][0] == 'X') winner = "p1";
        else if(board[0][0] == 'Y') winner = "p2";
    }
    if(board[2][0] == board[1][1] && board[2][0] == board[0][2])
    {
        if(board[2][0] == 'X') winner = "p1";
        else if(board[2][0] == 'Y') winner = "p2";
    }
    if(board[0][0] != ' ' && board[0][1] != ' ' && board[0][2] != ' ' && 
            board[1][0] != ' ' && board[1][1] != ' ' && board[1][2] != ' ' && 
            board[2][0] != ' ' && board[2][1] != ' ' && board[2][2] != ' ')
        winner = "n/a";
}

/**
 * This function outputs the tic tac toe board, as it is currently filled, to 
 * the console
 */
void Board::outputBoard()
{
    cout << "|===|===|===|" << endl <<
    "| " << board[0][0] << " | " << board[0][1] << " | " << board[0][2] << " |"; 
    cout << endl << "|===|===|===|" << endl <<
    "| " << board[1][0] << " | " << board[1][1] << " | " << board[1][2] << " |";
    cout << endl << "|===|===|===|" << endl <<
    "| " << board[2][0] << " | " << board[2][1] << " | " << board[2][2] << " |";  
    cout << endl << "|===|===|===|" << endl << endl;
}

/**
 * This function checks whether or not a choice location is valid
 * @param choice - space to check if filled on board
 * @return - whether or not said space is valid
 */
bool Board::checkChoice(int choice)
{
    bool isValid = false;
    if(choice == 1 && board[0][0] == ' ')
        isValid = true;
    if(choice == 2 && board[0][1] == ' ')
        isValid = true;
    if(choice == 3 && board[0][2] == ' ')
        isValid = true;
    if(choice == 4 && board[1][0] == ' ')
        isValid = true;
    if(choice == 5 && board[1][1] == ' ')
        isValid = true;
    if(choice == 6 && board[1][2] == ' ')
        isValid = true;
    if(choice == 7 && board[2][0] == ' ')
        isValid = true;
    if(choice == 8 && board[2][1] == ' ')
        isValid = true;
    if(choice == 9 && board[2][2] == ' ')
        isValid = true;
    return isValid;
}

/**
 * This function determines the location that the dumbAI will place its move
 * on the board
 * @return 
 */
int Board::dumbAIChoice()
{
    int cpuChoice;
    bool isValid = true;
    srand(time(0));
    do
    {
        cpuChoice = rand()%9+1;
        if(cpuChoice == 1 && board[0][0] != ' ')
            isValid = false;
        if(cpuChoice == 2 && board[0][1] != ' ')
            isValid = false;
        if(cpuChoice == 3 && board[0][2] != ' ')
            isValid = false;
        if(cpuChoice == 4 && board[1][0] != ' ')
            isValid = false;
        if(cpuChoice == 5 && board[1][1] != ' ')
            isValid = false;
        if(cpuChoice == 6 && board[1][2] != ' ')
            isValid = false;
        if(cpuChoice == 7 && board[2][0] != ' ')
            isValid = false;
        if(cpuChoice == 8 && board[2][1] != ' ')
            isValid = false;
        if(cpuChoice == 9 && board[2][2] != ' ')
            isValid = false;
    }while(!isValid);
    return cpuChoice;
}


/**
 * This function determines the smartAI's next move choice
 * @return - position on board to place smartAI's move
 */
int Board::smartAIChoice()
{      
    srand(time(0));
    int move = 0;
    if(board[1][1] == p1 && board[0][0] == ' ' && board[0][1] == ' '
            && board[0][2] == ' ' && board[1][0] == ' ' && board[1][2] == ' '
            && board[2][0] == ' ' && board[2][1] == ' ' && board[2][2] == ' ')
    {
        move = rand()%7;
        if(move = 0) move = 1;
        if(move = 1) move = 2;
        if(move = 2) move = 3;
        if(move = 3) move = 4;
        if(move = 4) move = 6;
        if(move = 5) move = 7;
        if(move = 6) move = 8;
        if(move = 7) move = 9;
    }
    else
    {
        for(int i = 0; i < 9; i++)
        {
            if(checkChoice(i))
            {
                move = i;
                return move;
            }
    }
    return move;
}
}


/**
 * This function has two users playing tic tac toe against each other;
 */
void player_vPlayer()
{
    bool quit = false;
    int pChoice;
    string winner = "";
    cout << "|===|===|===|" << endl
         << "| 1 | 2 | 3 |" << endl  
         << "|===|===|===|" << endl
         << "| 4 | 5 | 6 |" << endl
         << "|===|===|===|" << endl
         << "| 7 | 8 | 9 |" << endl 
         << "|===|===|===|" << endl << endl;
    Board b = Board();
    do
    {
        cout << "Player 1 choice: ";
        cin >> pChoice;
        cout << endl;
        while(!b.checkChoice(pChoice))
        {
            cout << endl << "Invalid choice! Choose again: ";
            cin >> pChoice;
            cout << endl;
        }
        b.player1Place(pChoice);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            cout << "Player 2 choice: ";
            cin >> pChoice;
            cout << endl;
            while(!b.checkChoice(pChoice))
            {
                cout << endl << "Invalid choice! Choose again: ";
                cin >> pChoice;
            }
            b.player2Place(pChoice);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

/**
 * This function has the user play tic tac toe against a dumbAI
 */
void player_vDumbAI()
{
    bool quit = false;
    int pChoice, cpuChoice;
    string winner = "";
    cout << "|===|===|===|" << endl
         << "| 1 | 2 | 3 |" << endl  
         << "|===|===|===|" << endl
         << "| 4 | 5 | 6 |" << endl
         << "|===|===|===|" << endl
         << "| 7 | 8 | 9 |" << endl 
         << "|===|===|===|" << endl << endl;
    Board b = Board();
    do
    {
        cout << "Player 1 choice: ";
        cin >> pChoice;
        cout << endl;
        while(!b.checkChoice(pChoice))
        {
            cout << endl << "Invalid choice! Choose again: ";
            cin >> pChoice;
            cout << endl;
        }
        b.player1Place(pChoice);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            if(winner == "p2") winner == "CPU";
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            cpuChoice = b.dumbAIChoice();
            cout << "CPU choice: " << cpuChoice << endl;
            cout << endl;
            b.player2Place(cpuChoice);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                if(winner == "p2") winner == "CPU";
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

/**
 * This function has the user play tic tac toe against a smartAI
 */
void player_vSmartAI()
{
    const int p1 = -1;
    const int p2 = 1;
    bool quit = false;
    int pChoice, cpuChoice;
    string winner = "";
    cout << "|===|===|===|" << endl
         << "| 1 | 2 | 3 |" << endl  
         << "|===|===|===|" << endl
         << "| 4 | 5 | 6 |" << endl
         << "|===|===|===|" << endl
         << "| 7 | 8 | 9 |" << endl 
         << "|===|===|===|" << endl << endl;
    Board b = Board();
    do
    {
        cout << "Player 1 choice: ";
        cin >> pChoice;
        cout << endl;
        while(!b.checkChoice(pChoice))
        {
            cout << endl << "Invalid choice! Choose again: ";
            cin >> pChoice;
            cout << endl;
        }
        b.player1Place(pChoice);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            if(winner == "p2") winner == "CPU";
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            cpuChoice = b.smartAIChoice();
            cout << "CPU choice: " << cpuChoice << endl;
            cout << endl;
            b.player2Place(cpuChoice);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                if(winner == "p2") winner == "CPU";
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

/**
 * This function has two dumbAI players play tic tac toe against each other
 */
void dumbAI_vDumbAI()
{
    srand(time(0));
    bool quit = false;
    int cpuChoice1, cpuChoice2;
    string winner = "";
    Board b = Board();
    do
    {
        do
        {
            cpuChoice1 = rand()%9+1;
        }while(!b.checkChoice(cpuChoice1));
        cout << "CPU 1 choice: " << cpuChoice1 << endl;
        cout << endl;
        b.player1Place(cpuChoice1);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            if(winner == "p2") winner == "CPU 2";
            if(winner == "p1") winner == "CPU 1";
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            do
            {
                cpuChoice2 = rand()%9+1;
            }while(!b.checkChoice(cpuChoice2));
            cout << "CPU 2 choice: " << cpuChoice2 << endl;
            cout << endl;
            b.player2Place(cpuChoice2);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                if(winner == "p2") winner == "CPU 2";
                if(winner == "p1") winner == "CPU 1";
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

/**
 * Code constructor
 */
Code::Code()
{
    size = 0;
}

/**
 * This function sets the size of the code
 * @param size
 */
void Code::setSize(int size)
{
    this->size = size;
}

/**
 * This function generates a random code to be deciphered
 */
void Code::makeCode()
{
    srand(time(0));
    for(int i = 0; i < size; i++)
    {
        code.push_back(rand()%10);
        progress.push_back(' ');
    }
}

/**
 * This function takes in guess input from the user and places it into a vector
 * @param input
 */
void Code::makeGuess(int input)
{
    guess.push_back(input);
}

/**
 * This function evaluates the vector containing the user's guess and constructs
 * the progress vector to reflect which of the user's guesses were correct, in
 * the wrong spot, or incorrect entirely.
 */
void Code::makeProgress()
{
    for(int i = 0; i < size; i++)
    {
        if(guess[i] == code[i]) progress[i] = '!';
        else
        {
            if(guess[i] != code[i] && (guess[i] == code[0] 
                    || guess[i] == code[1] || guess[i] == code[2]
                    || guess[i] == code[3] || guess[i] == code[4]
                    || guess[i] == code[5] || guess[i] == code[6]))
            {
                progress[i] = '?';
            }
            else
                progress[i] = 'X';
        }
    }
    
}

/**
 * This function outputs the randomly generated code
 */
void Code::outputCode()
{
    cout << "Code: ";
    for(int i = 0; i < size; i++)
    {
        cout << "[" << code[i] << "] ";
    }
    cout << endl;
}

/**
 * This function outputs the vector containing the user's guess input
 */
void Code::outputGuess()
{
    cout << "Guess: ";
    for(int i = 0; i < size; i++)
    {
        cout << "[" << guess[i] << "] ";
    }
    cout << endl;
}

/**
 * This function outputs the Progress vector (which tells the user which
 * of their guesses were correct, in the wrong spot, or entirely incorrect)
 */
void Code::outputProgress()
{
    cout << "Progress: ";
    for(int i = 0; i < size; i++)
    {
        cout << "[" << progress[i] << "] ";
    }
    cout << endl;
}

/**
 * This function plays the Codebreaker game after setting up the initial
 * parameters
 * @param size - size of the code to be randomly generated
 */
void playGame(int size)
{
    Code game = Code();
    game.setSize(size);
    game.makeCode();
    
    int input; int turn = 0;
    do
    {
        cout << "Input a " << size << "-digit guess, each digit separated by"
                << " a space: ";
        for(int i = 0; i < size; i++)
        {
            cin >> input;
            game.makeGuess(input);
        }
        game.makeProgress();

        game.outputGuess();
        game.resetGuess();
        game.outputProgress();
        game.incTurn();
    }while(!game.winStatus() && game.getTurn() < 10);
    
    if(game.winStatus())
    {
        cout << "You win!" << endl << endl;
    }
    else
    {
        game.outputCode();
        cout << "You lose!" << endl << endl;
    }
}

/**
 * This function determines whether the user has won the game
 * @return - whether or not the user has won
 */
bool Code::winStatus()
{
    bool win = true;
    for(int i = 0; i < size; i++)
    {
        if(guess[i] != code[i])
            win = false;
    }
    return win;
}

/**
 * This function resets the vector containing the user's guess
 */
void Code::resetGuess()
{
    for(int i = 0; i < size; i++)
    {
        guess.pop_back();
    }
}

/**
 * This function increases the turn counter
 */
void Code::incTurn()
{
    turn++;
}

/**
 * This function gets the current turn
 * @return - current turn
 */
int Code::getTurn()
{
    return turn;
}