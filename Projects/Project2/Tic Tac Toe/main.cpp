//        |===|===|===|
//        |   |   |   |  
//        |===|===|===|
//        |   |   |   |  
//        |===|===|===|
//        |   |   |   |  
//        |===|===|===|

#include <cstdlib>
#include <iostream>

using namespace std;

class Board
{
private:
    char** board; //The board
    char p1; //The character seen on the board for player's choice
    char p2; //The character seen on the board for player's choice
public:
    Board();
    void player1Place(int);
    void player2Place(int);
    bool checkChoice(int);
    int dumbAIChoice();
    int smartAIChoice(char);
    void checkBoard(string&);
    void outputBoard();
};

void player_vPlayer();
void player_vDumbAI();
void player_vSmartAI();
void dumbAI_vDumbAI();
/*
 * 
 */
int main(int argc, char** argv) {

    int choice;
    cout << "TIC TAC TOE!" << endl
            << "    1. Player vs. Player" << endl
            << "    2. Player vs. Dumb AI" << endl
            << "    3. Player vs. Smart AI" << endl
            << "    4. Dumb AI vs. Dumb AI" << endl
            << "Your choice: ";
    cin >> choice;
    cout << endl;
    switch(choice)
    {
        case 1:
            player_vPlayer();
            break;
        case 2:
            player_vDumbAI();
            break;
        case 3:
            player_vSmartAI();
            break;
        case 4:
            dumbAI_vDumbAI();
            break;
        default:
            cout << "Invalid Input!";
    }
    return 0;
}

Board::Board()
{
    board = new char*[3];
    for(int i = 0; i < 3; i++)
        board[i] = new char[3];
        
    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            board[i][j] = ' ';
        }
    }  
    p1 = 'X';
    p2 = 'O';
}

void Board::player1Place(int choice)
{
    if(choice == 1 && board[0][0] == ' ')
        board[0][0] = p1;
    if(choice == 2 && board[0][1] == ' ')
        board[0][1] = p1;
    if(choice == 3 && board[0][2] == ' ')
        board[0][2] = p1;
    if(choice == 4 && board[1][0] == ' ')
        board[1][0] = p1;
    if(choice == 5 && board[1][1] == ' ')
        board[1][1] = p1;
    if(choice == 6 && board[1][2] == ' ')
        board[1][2] = p1;
    if(choice == 7 && board[2][0] == ' ')
        board[2][0] = p1;
    if(choice == 8 && board[2][1] == ' ')
        board[2][1] = p1;
    if(choice == 9 && board[2][2] == ' ')
        board[2][2] = p1;
}

void Board::player2Place(int choice)
{
    if(choice == 1 && board[0][0] == ' ')
        board[0][0] = p2;
    if(choice == 2 && board[0][1] == ' ')
        board[0][1] = p2;
    if(choice == 3 && board[0][2] == ' ')
        board[0][2] = p2;
    if(choice == 4 && board[1][0] == ' ')
        board[1][0] = p2;
    if(choice == 5 && board[1][1] == ' ')
        board[1][1] = p2;
    if(choice == 6 && board[1][2] == ' ')
        board[1][2] = p2;
    if(choice == 7 && board[2][0] == ' ')
        board[2][0] = p2;
    if(choice == 8 && board[2][1] == ' ')
        board[2][1] = p2;
    if(choice == 9 && board[2][2] == ' ')
        board[2][2] = p2;
}

void Board::checkBoard(string& winner)
{
    if(board[0][0] == board[0][1] && board[0][0] == board[0][2])
    {
        if(board[0][0] == 'X') winner = "p1";
        else if(board[0][0] == 'Y') winner = "p2";
    }
    if(board[1][0] == board[1][1] && board[1][0] == board[1][2])
    {
        if(board[1][0] == 'X') winner = "p1";
        else if(board[1][0] == 'Y') winner = "p2";
    }
    if(board[2][0] == board[2][1] && board[2][0] == board[2][2])
    {
        if(board[2][0] == 'X') winner = "p1";
        else if(board[2][0] == 'Y') winner = "p2";
    }
    if(board[0][0] == board[1][0] && board[0][0] == board[2][0])
    {
        if(board[0][0] == 'X') winner = "p1";
        else if(board[0][0] == 'Y') winner = "p2";
    }
    if(board[0][1] == board[1][1] && board[0][1] == board[2][1])
    {
        if(board[0][1] == 'X') winner = "p1";
        else if(board[0][1] == 'Y') winner = "p2";
    }
    if(board[0][2] == board[1][2] && board[0][2] == board[2][2])
    {
        if(board[0][2] == 'X') winner = "p1";
        else if(board[0][2] == 'Y') winner = "p2";
    }
    if(board[0][0] == board[1][1] && board[0][0] == board[2][2])
    {
        if(board[0][0] == 'X') winner = "p1";
        else if(board[0][0] == 'Y') winner = "p2";
    }
    if(board[2][0] == board[1][1] && board[2][0] == board[0][2])
    {
        if(board[2][0] == 'X') winner = "p1";
        else if(board[2][0] == 'Y') winner = "p2";
    }
    if(board[0][0] != ' ' && board[0][1] != ' ' && board[0][2] != ' ' && 
            board[1][0] != ' ' && board[1][1] != ' ' && board[1][2] != ' ' && 
            board[2][0] != ' ' && board[2][1] != ' ' && board[2][2] != ' ')
        winner = "n/a";
}

void Board::outputBoard()
{
    cout << "|===|===|===|" << endl <<
    "| " << board[0][0] << " | " << board[0][1] << " | " << board[0][2] << " |"; 
    cout << endl << "|===|===|===|" << endl <<
    "| " << board[1][0] << " | " << board[1][1] << " | " << board[1][2] << " |";
    cout << endl << "|===|===|===|" << endl <<
    "| " << board[2][0] << " | " << board[2][1] << " | " << board[2][2] << " |";  
    cout << endl << "|===|===|===|" << endl << endl;
}

bool Board::checkChoice(int choice)
{
    bool isValid = false;
    if(choice == 1 && board[0][0] == ' ')
        isValid = true;
    if(choice == 2 && board[0][1] == ' ')
        isValid = true;
    if(choice == 3 && board[0][2] == ' ')
        isValid = true;
    if(choice == 4 && board[1][0] == ' ')
        isValid = true;
    if(choice == 5 && board[1][1] == ' ')
        isValid = true;
    if(choice == 6 && board[1][2] == ' ')
        isValid = true;
    if(choice == 7 && board[2][0] == ' ')
        isValid = true;
    if(choice == 8 && board[2][1] == ' ')
        isValid = true;
    if(choice == 9 && board[2][2] == ' ')
        isValid = true;
    return isValid;
}


int Board::dumbAIChoice()
{
    int cpuChoice;
    bool isValid = true;
    srand(time(0));
    do
    {
        cpuChoice = rand()%9+1;
        if(cpuChoice == 1 && board[0][0] != ' ')
            isValid = false;
        if(cpuChoice == 2 && board[0][1] != ' ')
            isValid = false;
        if(cpuChoice == 3 && board[0][2] != ' ')
            isValid = false;
        if(cpuChoice == 4 && board[1][0] != ' ')
            isValid = false;
        if(cpuChoice == 5 && board[1][1] != ' ')
            isValid = false;
        if(cpuChoice == 6 && board[1][2] != ' ')
            isValid = false;
        if(cpuChoice == 7 && board[2][0] != ' ')
            isValid = false;
        if(cpuChoice == 8 && board[2][1] != ' ')
            isValid = false;
        if(cpuChoice == 9 && board[2][2] != ' ')
            isValid = false;
    }while(!isValid);
    return cpuChoice;
}


int Board::smartAIChoice()
{
    int test;
    int temp = -2;
    string winner = "";
    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            //I think this doesn't work because I'm trying to assign a value to
            //board[i][j], and then reassign it with recursion, but I'm not sure
            //how exactly to fix it. I think the logic of this is sound, though.
            board[i][j] == player;
            test = smartAIChoice(p1);
            if(test > temp)
            {
                temp = test;
            }
            board[i][j] == ' ';
        }
                
    }
    return temp;
}

void player_vPlayer()
{
    bool quit = false;
    int pChoice;
    string winner = "";
    cout << "|===|===|===|" << endl
         << "| 1 | 2 | 3 |" << endl  
         << "|===|===|===|" << endl
         << "| 4 | 5 | 6 |" << endl
         << "|===|===|===|" << endl
         << "| 7 | 8 | 9 |" << endl 
         << "|===|===|===|" << endl << endl;
    Board b = Board();
    do
    {
        cout << "Player 1 choice: ";
        cin >> pChoice;
        cout << endl;
        while(!b.checkChoice(pChoice))
        {
            cout << endl << "Invalid choice! Choose again: ";
            cin >> pChoice;
            cout << endl;
        }
        b.player1Place(pChoice);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            cout << "Player 2 choice: ";
            cin >> pChoice;
            cout << endl;
            while(!b.checkChoice(pChoice))
            {
                cout << endl << "Invalid choice! Choose again: ";
                cin >> pChoice;
            }
            b.player2Place(pChoice);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

void player_vDumbAI()
{
    bool quit = false;
    int pChoice, cpuChoice;
    string winner = "";
    cout << "|===|===|===|" << endl
         << "| 1 | 2 | 3 |" << endl  
         << "|===|===|===|" << endl
         << "| 4 | 5 | 6 |" << endl
         << "|===|===|===|" << endl
         << "| 7 | 8 | 9 |" << endl 
         << "|===|===|===|" << endl << endl;
    Board b = Board();
    do
    {
        cout << "Player 1 choice: ";
        cin >> pChoice;
        cout << endl;
        while(!b.checkChoice(pChoice))
        {
            cout << endl << "Invalid choice! Choose again: ";
            cin >> pChoice;
            cout << endl;
        }
        b.player1Place(pChoice);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            if(winner == "p2") winner == "CPU";
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            cpuChoice = b.dumbAIChoice();
            cout << "CPU choice: " << cpuChoice << endl;
            cout << endl;
            b.player2Place(cpuChoice);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                if(winner == "p2") winner == "CPU";
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

void player_vSmartAI()
{
    bool quit = false;
    int pChoice, cpuChoice;
    string winner = "";
    cout << "|===|===|===|" << endl
         << "| 1 | 2 | 3 |" << endl  
         << "|===|===|===|" << endl
         << "| 4 | 5 | 6 |" << endl
         << "|===|===|===|" << endl
         << "| 7 | 8 | 9 |" << endl 
         << "|===|===|===|" << endl << endl;
    Board b = Board();
    do
    {
        cout << "Player 1 choice: ";
        cin >> pChoice;
        cout << endl;
        while(!b.checkChoice(pChoice))
        {
            cout << endl << "Invalid choice! Choose again: ";
            cin >> pChoice;
            cout << endl;
        }
        b.player1Place(pChoice);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            if(winner == "p2") winner == "CPU";
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            cpuChoice = b.smartAIChoice('O');
            cout << "CPU choice: " << cpuChoice << endl;
            cout << endl;
            b.player2Place(cpuChoice);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                if(winner == "p2") winner == "CPU";
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

void dumbAI_vDumbAI()
{
    srand(time(0));
    bool quit = false;
    int cpuChoice1, cpuChoice2;
    string winner = "";
    Board b = Board();
    do
    {
        do
        {
            cpuChoice1 = rand()%9+1;
        }while(!b.checkChoice(cpuChoice1));
        cout << "CPU 1 choice: " << cpuChoice1 << endl;
        cout << endl;
        b.player1Place(cpuChoice1);
        b.outputBoard();
        b.checkBoard(winner);
        if(winner == "p1" || winner == "p2" || winner == "n/a" )
        {
            if(winner == "p2") winner == "CPU 2";
            if(winner == "p1") winner == "CPU 1";
            cout << "Winner: " << winner << endl;
            quit = true;
        }
        if(!quit)
        {
            do
            {
                cpuChoice2 = rand()%9+1;
            }while(!b.checkChoice(cpuChoice2));
            cout << "CPU 2 choice: " << cpuChoice2 << endl;
            cout << endl;
            b.player2Place(cpuChoice2);
            b.outputBoard();
            b.checkBoard(winner);
            if(winner == "p1" || winner == "p2" || winner == "n/a")
            {
                if(winner == "p2") winner == "CPU 2";
                if(winner == "p1") winner == "CPU 1";
                cout << "Winner: " << winner << endl;
                quit = true;
            }
        }
    }while (!quit);
}

