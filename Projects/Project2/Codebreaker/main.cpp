/* 
 * File:   main.cpp
 * Author: Jonathan
 *
 * Created on December 13, 2015, 6:08 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

class Code
{
private:
    int size;
    int turn;
    vector<int> code;
    vector<int> guess;
    vector<char> progress;
public:
    Code();
    void setSize(int);
    void makeCode();
    void makeGuess(int);
    void makeProgress();
    bool winStatus();
    void outputCode();
    void outputGuess();
    void outputProgress();
    void resetGuess();
    void incTurn();
    int getTurn();
};

void playGame(int size);
/*
 * 
 */
int main(int argc, char** argv) {

    cout << "   CODEBREAKER!" << endl << endl;
    
    cout << "The computer will generate a code and it is up to you to decipher"
            << " it!\nInput your guess, and the computer will tell you:" << endl
            << "     - Which numbers are correct [!]" << endl
            << "     - Which numbers are in the code, but in a different spot"
            << " [?]" << endl
            << "     - Which numbers are incorrect [X]" << endl << endl;
    
    int choice;
    cout << "Choose your difficulty:" << endl
            << "    1. Easy" << endl
            << "    2. Medium" << endl
            << "    3. Hard" << endl << endl
            << "Your choice: ";
    cin >> choice;
    int size = 0;
    switch(choice)
    {
        case 1:
            size = 4;
            playGame(size);
            break;
        case 2:
            size = 5;
            playGame(size);
            break;
        case 3:
            size = 6;
            playGame(size);
            break;
        default:
            cout << "Invalid input!";
    } 
    cout << endl;
    
    return 0;
}

Code::Code()
{
    size = 0;
}

void Code::setSize(int size)
{
    this->size = size;
}

void Code::makeCode()
{
    srand(time(0));
    for(int i = 0; i < size; i++)
    {
        code.push_back(rand()%10);
        progress.push_back(' ');
    }
}

void Code::makeGuess(int input)
{
    guess.push_back(input);
}

void Code::makeProgress()
{
    for(int i = 0; i < size; i++)
    {
        if(guess[i] == code[i]) progress[i] = '!';
        else
        {
            if(guess[i] != code[i] && (guess[i] == code[0] 
                    || guess[i] == code[1] || guess[i] == code[2]
                    || guess[i] == code[3] || guess[i] == code[4]
                    || guess[i] == code[5] || guess[i] == code[6]))
            {
                progress[i] = '?';
            }
            else
                progress[i] = 'X';
        }
    }
    
}

void Code::outputCode()
{
    cout << "Code: ";
    for(int i = 0; i < size; i++)
    {
        cout << "[" << code[i] << "] ";
    }
    cout << endl;
}

void Code::outputGuess()
{
    cout << "Guess: ";
    for(int i = 0; i < size; i++)
    {
        cout << "[" << guess[i] << "] ";
    }
    cout << endl;
}

void Code::outputProgress()
{
    cout << "Progress: ";
    for(int i = 0; i < size; i++)
    {
        cout << "[" << progress[i] << "] ";
    }
    cout << endl;
}

void playGame(int size)
{
    Code game = Code();
    game.setSize(size);
    game.makeCode();
    
    int input; int turn = 0;
    do
    {
        cout << "Input a " << size << "-digit guess, each digit separated by"
                << " a space: ";
        for(int i = 0; i < size; i++)
        {
            cin >> input;
            game.makeGuess(input);
        }
        game.makeProgress();

        game.outputGuess();
        game.resetGuess();
        game.outputProgress();
        game.incTurn();
    }while(!game.winStatus() && game.getTurn() < 10);
    
    if(game.winStatus())
    {
        cout << "You win!" << endl << endl;
    }
    else
    {
        game.outputCode();
        cout << "You lose!" << endl << endl;
    }
}

bool Code::winStatus()
{
    bool win = true;
    for(int i = 0; i < size; i++)
    {
        if(guess[i] != code[i])
            win = false;
    }
    return win;
}

void Code::resetGuess()
{
    for(int i = 0; i < size; i++)
    {
        guess.pop_back();
    }
}

void Code::incTurn()
{
    turn++;
}

int Code::getTurn()
{
    return turn;
}