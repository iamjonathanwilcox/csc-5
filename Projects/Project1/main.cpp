/* 
 * File:   main.cpp
 * Author: Jonathan
 *
 * Created on October 16, 2015, 7:18 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/**
 * This is a helper function for the Roulette game that establishes the bets
 * being placed by the user
 * @param chips - Takes in the total chips the player has
 * @param numBet - How many chips the user is placing on a single-number bet
 * @param numVal - Which single number the user is betting on
 * @param columnBet - How many chips the user is placing on a column bet
 * @param columnVal - Which column the user is betting on
 * @param row6Bet - How many chips the user is placing on a 6-row bet
 * @param row6Val - Which set of 6 rows the user is betting on
 * @param row4Bet - How many chips the user is placing on a 4-row bet
 * @param row4Val - Which set of 4 rows the user is betting on
 * @param row1Bet - How many chips the user is betting on a single row
 * @param row1Val - Which row the user is betting on
 * @param colorBet - How many chips the user is betting on a particular color
 * @param colorVal - Which color the user is betting on
 * @param oddEvenBet - How many chips the user bets on an odd or even value
 * @param oddEvenVal - Which value chosen by user for bet (odd or even)
 */
void PlaceBets(int chips, int&numBet, int&numVal, int&columnBet, int&columnVal,
        int&row6Bet, int&row6Val, int&row4Bet, int&row4Val, int&row1Bet,
        int&row1Val, int&colorBet, int&colorVal, int&oddEvenBet, int&oddEvenVal)
{
    //User inputs which type of bet they wish to place, then the value for the
    //amount of chips they wish to bet and the value for the actual item they
    //are betting on.
    int response, choice;
    do
    {
        cout << endl << "You can place the following bets:" << endl
            << "    1. Single-number (Odds 35:1)"<< endl
            << "    2. Column (Odds 2:1)" << endl
            << "    3. Six Rows (Odds 1:1)" << endl
            << "    4. Four Rows (Odds 2:1)" << endl
            << "    5. Single Row (Odds 5:1)" << endl
            << "    6. Color(Odds 1:1)" << endl
            << "    7. Odd/Even (Odds 2:1)" << endl << endl
                << "Enter your choice: ";
        cin >> choice;
        cout << endl << endl;
        if(choice == 1 && numBet == 0)
        {
            cout << "Number you wish to bet on: ";
            cin >> numVal;
            do
            {
                cout << "Amount of chips you wish to bet: ";
                cin >> numBet;
                if(numBet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(numBet > chips);
            chips = chips - numBet;
        }
        else if(choice == 2 && columnBet == 0)
        {
            cout << "Column 1:  1   4   7   10  13  16  19  22  25  28  31  34"
                    << endl
                    << "Column 2:   2   5   8   11  14  17  20  23  26  29  32"
                    "  35" << endl
                    << "Column 3:   3   6   9   12  15  18  21  24  27  30  33"
                    "  36" << endl << endl;

            cout << "Column you wish to bet on: " << endl;
            cin >> columnVal;
            do
            {
                cout << "Amount of chips you wish to bet: " << endl;
                cin >> columnBet;
                if(columnBet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(columnBet > chips);
            chips = chips - columnBet;
        }
        else if(choice == 3 && row6Bet == 0)
        {
            cout << "1st Six Rows: 1  2  3" << endl
                    << "              4  5  6" << endl
                    << "              7  8  9" << endl
                    << "              10 11 12" << endl
                    << "              13 14 15" << endl
                    << "              16 17 18" << endl << endl
                    << "2nd Six Rows: 19 20 21" << endl
                    << "              22 23 24" << endl
                    << "              25 26 27" << endl
                    << "              28 29 30" << endl
                    << "              31 32 33" << endl
                    << "              34 35 36" << endl << endl;

            cout << "Set of rows you wish to bet on: " << endl;
            cin >> row6Val;
            do
            {
                cout << "Amount of chips you wish to bet: " << endl;
                cin >> row6Bet;
                if(row6Bet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(row6Bet > chips);
            chips = chips - row6Bet;
        }
        else if(choice == 4 && row4Bet == 0)
        {
            cout << "1st Four Rows: 1  2  3" << endl
                    << "               4  5  6" << endl
                    << "               7  8  9" << endl
                    << "               10 11 12" << endl << endl
                    << "2nd Four Rows: 13 14 15" << endl
                    << "               16 17 18" << endl
                    << "               19 20 21" << endl
                    << "               22 23 24" << endl << endl
                    << "3rd Four Rows: 25 26 27" << endl
                    << "               28 29 30" << endl
                    << "               31 32 33" << endl
                    << "               34 35 36" << endl << endl;
            
            cout << "Set of rows you wish to bet on: " << endl;
            cin >> row4Val;
            do
            {
                cout << "Amount of chips you wish to bet: " << endl;
                cin >> row4Bet;
                if(row4Bet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(row4Bet > chips);
            chips = chips - row4Bet;
        }
        else if(choice == 5 && row4Bet == 0)
        {
            cout << "Row you wish to bet on: " << endl;
            cin >> row1Val;
            do
            {
                cout << "Amount of chips you wish to bet: " << endl;
                cin >> row1Bet;
                if(row1Bet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(row1Bet > chips);
            chips = chips - row1Bet;
        }
        else if(choice == 6  && row1Bet == 0)
        {
            cout << "Enter the number you wish to bet on: " << endl
                    << "    1. Reds" << endl
                    << "    2. Blacks" << endl;
            cin >> colorVal;
           
            do
            {
                cout << "Amount of chips you wish to bet: " << endl;
                cin >> colorBet;
                if(colorBet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(colorBet > chips && colorBet == 0);
            chips = chips - colorBet;
        }
        else if(choice == 7 && oddEvenBet == 0)
        {
            cout << "Do you wish to bet on odds or evens: " << endl
                    << "    1. Odds" << endl
                    << "    2. Evens" << endl;
            cin >> oddEvenVal;
                       
            do
            {
                cout << "Amount of chips you wish to bet: " << endl;
                cin >> oddEvenBet;
                if(oddEvenBet > chips)
                    cout << "Invalid bet." << endl;
            }
            while(oddEvenBet > chips);
            chips = chips - oddEvenBet;
        }
        else
        {
            cout << "Invalid choice." << endl << endl;
        }
        cout << "Would you like to place another bet?" << endl
                << "    1. Yes" << endl
                << "    2. No" << endl << endl
                << "Your choice: ";
        cin >> response;
    }
    while(chips > 0 && response != 2);
}

    //1   4   7   10  13  16  19  22  25  28  31  34    | 3
    //2   5   8   11  14  17  20  23  26  29  32  35    | 2
    //3   6   9   12  15  18  21  24  27  30  33  36    | 1
    //__  __  __  __  __  __  __  __  __  __  __  __
    //1   2   3   4   5   6   7   8   9   10  11  12
    
    //12 Rows
    //3 Columns


/**
 * This is a helper function for the Roulette game that determines which row the
 * spun number coincides with.
 * @param spin - Takes in the raw spin value
 * @return - Returns the integer value of which row the number falls in
 */
int Row(int spin)
{
    //Determines the row of the number spun
    int row;
    if(spin == 1 || spin == 2 || spin == 3)
        row = 1;
    else if(spin == 4 || spin == 5 || spin == 6)
        row = 2;
    else if(spin == 7 || spin == 8 || spin == 9)
        row = 3;
    else if(spin == 10 || spin == 11 || spin == 12)
        row = 4;
    else if(spin == 13 || spin == 14 || spin == 15)
        row = 5;
    else if(spin == 16 || spin == 17 || spin == 18)
        row = 6;
    else if(spin == 19 || spin == 20 || spin == 21)
        row = 7;
    else if(spin == 22 || spin == 23 || spin == 24)
        row = 8;
    else if(spin == 25 || spin == 26 || spin == 27)
        row = 9;
    else if(spin == 28 || spin == 29 || spin == 30)
        row = 10;
    else if(spin == 31 || spin == 32 || spin == 33)
        row = 11;
    else if(spin == 34 || spin == 35 || spin == 36)
        row = 12;
    else
        row = 0;
    return row;
}

/**
 * This is a helper function that determines which column the spun number fits
 * into in the Roulette game.
 * @param spin - takes in the raw spin number
 * @return - returns the integer value of the column
 */
int Column(int spin)
{
    //Determines the column of the number spun
    int column;
    if(spin == 1 || spin == 4|| spin == 7 || spin == 10 || spin == 13
            || spin == 16 || spin == 19 || spin == 22 || spin == 25
            || spin == 28 || spin == 31 || spin == 34)
    {
        column = 1;
        return column;
    }
    else if(spin == 2 || spin == 5 || spin == 8 || spin == 11 || spin == 14 
            || spin == 17 || spin == 20 || spin == 23 || spin == 26
            || spin == 29 || spin == 32 || spin == 35)
    {
        column = 2;
        return column;
    }
    else if(spin == 3 || spin == 6 || spin == 9 || spin == 12 || spin == 15 
            || spin == 18 || spin == 21 || spin == 24 || spin == 27 
            || spin == 30 || spin == 33 || spin == 36)
    {
        column = 3;
        return column;
    }
    else
    {
        column = 0;
        return column;
    }
}

/**
 * This function is a helper function to determine the numerical value of the
 * roulette spin.
 * @param spin - Takes the initial, unaltered random roulette spin.
 * @return - Returns the correct spin value, accounting for the value 00 which
 * cannot be assigned automatically.
 */
int Number(int spin)
{
    //Determines the number spun
    int num;
    if(spin != 37)
        num = spin;
    else
        num = 00;
    return num;
    
}

/**
 * This function is a helper function for the Roulette game.
 * @param spin - Takes in the roulette spin to determine the color value.
 * @return - Returns the color value in string form.
 */
string Color(int spin)
{
    //Determines the color of the number spun
    string color;
    if(spin == 0 || spin == 37)
    {
	color = "Green";
    }
    else if((spin >= 1 && spin <= 10) || (spin >= 19 && spin <= 28))
    {
	if(spin%2 == 0)
            color = "Black";
	else
            color = "Red";
    }
    else if((spin >= 11 && spin <= 18) || (spin >= 29 && spin <= 36))
    {
	if(spin%2 == 0)
            color = "Red";
	else
            color = "Black";
    }
    return color;
}     

/**
 * This function receives the value for each individual bet input by the user
 * and 
 * @param chips - number of chips the player will have after bets
 * @param numBet - number of chips bet on single number
 * @param numVal - value single number bet was placed on
 * @param num - number value that was spun
 * @param columnBet - number of chips bet on a column
 * @param columnVal - value column bet was placed on
 * @param column - column value that was spun
 * @param row6Bet - number of chips bet on a set of 6 rows
 * @param row6Val - value for the set of 6 rows bet was placed on
 * @param row - row value that was spun
 * @param row4Bet - number of chips bet on a set of 4 rows
 * @param row4Val - value for the set of 4 rows bet was placed on
 * @param row1Bet - number of chips bet on a single row
 * @param row1Val - value for the single row bet was placed on
 * @param colorBet - number of chips bet on a color
 * @param colorVal - value for the color bet was placed on
 * @param color - color value that was spun
 * @param oddEvenBet - number of chips bet on either odd or even value
 * @param oddEvenVal - odd or even value that bet was placed on
 */
void EvalBets(int&chips, int numBet, int numVal, int num, int columnBet,
        int columnVal, int column, int row6Bet,int row6Val, int row,
        int row4Bet, int row4Val, int row1Bet, int row1Val, int colorBet,
            int colorVal, string color, int oddEvenBet, int oddEvenVal)
{
    if(numBet > 0)
    {
        cout << "You bet " << numBet << " chips on number " << numVal << "."
                << endl;
        if(num = numVal)
        {
            cout << "You win!" << endl;
            chips = chips + (numBet*35);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - numBet;
        }
    }
    if(columnBet > 0)
    {
        cout << "You bet " << columnBet << " chips on column " << columnVal
                << "." << endl;
        if(column = columnVal)
        {
            cout << "You win!" << endl;
            chips = chips + (columnBet*2);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - columnBet;
        }
    }
    if(row6Bet > 0)
    {
        cout << "You bet " << row6Bet << " chips on set " << row6Val
                << " of six rows." << endl;
        if(((row >= 1 && row <= 6) && row6Val == 1)
                || ((row >= 7 && row <= 12) && row6Val == 2))
        {
            cout << "You win!" << endl;
            chips = chips + (row6Bet*1);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - row6Bet;
        }
    }
    if(row4Bet > 0)
    {
        cout << "You bet " << row4Bet << " chips on set " << row4Val
                << " of four rows." << endl;
        if(((row >= 1 && row <= 4) && row4Val == 1)
                || ((row >= 5 && row <= 8) && row4Val == 2)
                || ((row >= 9 && row <= 12) && row4Val == 3))
        {
            cout << "You win!" << endl;
            chips = chips + (row4Bet*2);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - row4Bet;
        }
    }
    if(row1Bet > 0)
    {
        cout << "You bet " << row1Bet << " chips on row " << row1Val << "."
                << endl;
        if(row = row1Val)
        {
            cout << "You win!" << endl;
            chips = chips + (row1Bet*5);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - row1Bet;
        }
    }
    if(colorBet > 0)
    {
        if(colorVal == 2)
        {
            cout << "You bet " << colorBet << " chips on the color black."
                    << endl;
        }
        else if(colorVal == 1)
        {
            cout << "You bet " << colorBet << " chips on the color red."
                    << endl;
        }
         
        if((tolower(color[0]) == 'r' && colorVal == 1)
                || (tolower(color[0]) == 'b' && colorVal == 2))
        {
            cout << "You win!" << endl;
            chips = chips + (colorBet*1);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - colorBet;
        }
    }
    if(oddEvenBet > 0)
    {
        
        if(oddEvenVal == 1)
        {
            cout << "You bet " << oddEvenBet << " chips on odds."
                << endl;
        }
        else if(oddEvenVal == 2)
        {
            cout << "You bet " << oddEvenBet << " chips on evens."
                << endl;    
        }
                
        if((num%2 == 0 && oddEvenVal == 2) || (num%2 != 0 && oddEvenVal == 1))
        {
            cout << "You win!" << endl;
            chips = chips + (oddEvenBet*1);
        }
        else
        {
            cout << "You lose." << endl;
            chips = chips - oddEvenBet;
        }
    }
}

/**
 * This function is the base game of Roulette.
 * @param chips - Takes in the number of chips the player has available and
 * modifies it according to how the function plays out.
 */
void Roulette(int&chips)
{
    //All bet values - which are separate variables due to them each having
    //uniquely weighted odds - are set to zero by default, and altered when the
    //PlaceBets function is called
    
    int numBet=0, numVal=0, columnBet=0, columnVal=0, row6Bet=0, row6Val=0,
            row4Bet=0, row4Val=0, row1Bet=0, row1Val=0, colorBet=0, colorVal=0,
            oddEvenBet=0, oddEvenVal=0;
    PlaceBets(chips, numBet, numVal, columnBet, columnVal, row6Bet, row6Val,
            row4Bet, row4Val, row1Bet, row1Val, colorBet, colorVal, oddEvenBet,
            oddEvenVal);
    
    //The roulette is spun and its details are determined by a random number
    //1-37 (which accounts for 0 and 00) by putting it through several functions
    
    int spin = rand()%38;
    string color = Color(spin);
    int num = Number(spin), column = Column(spin), row = Row(spin);
        
    cout << endl << "Your Roll:" << endl
            << " - Number: " << num << endl
            << " - Color: " << color << endl
            << " - Column: " << column << endl
            << " - Row: " << row << endl << endl;
    
    //The bet values are compared to the rolled values. If a bet was placed, the
    //values are compared. If no bet was placed, the values are ignored.
    //The total chip value is modified to reflect the bet outcomes.
    //This is all done through the EvalBets function.
    
    EvalBets(chips, numBet, numVal, num, columnBet, columnVal, column, row6Bet,
            row6Val, row, row4Bet, row4Val, row1Bet, row1Val, colorBet,
            colorVal, color, oddEvenBet, oddEvenVal);
}

/**
 * This function is the base game of Craps.
 * @param chips - Takes in the total number of chips that the user has
 * available.
 */
void Craps(int&chips)
{
    int bet, response;
    do
    {
        //User inputs bet
        
        cout << "Enter amount of chips you wish to bet: ";
        cin >> bet;
        if(bet > chips)
        {
            while(!(bet <= chips))
            {
                cout << "Invalid bet. Please enter another: ";
                cin >> bet;
            }
        }
        cout << endl << endl;
        
        //Initial dice roll;
        int dice1 = rand()%6+1;
        int dice2 = rand()%6+1;
        
        cout << "ROUND 1:" << endl
                << "Dice 1: " << dice1 << endl
                << "Dice 2: " << dice2 << endl 
                << "Sum: " << dice1 + dice2 << endl << endl;
        
        //Evaluation of round 1 dice roll
        if(dice1 + dice2 == 7 || dice1 + dice2 == 11)
        {
            cout << "You win!" << endl;
            chips += bet;
        }
        else if(dice1 + dice2 == 2 || dice1 + dice2 == 12 || dice1 + dice2 == 3)
        {
            cout << "You lose." << endl;
            chips -= bet;
        }
        else
        {
            int dice3 = rand()%6+1;
            int dice4 = rand()%6+1;
            cout << "ROUND 2:" << endl
                    << "Dice 1: " << dice3 << endl
                    << "Dice 2: " << dice4 << endl
                    << "Sum: " << dice3 + dice4 << endl << endl;
            
            //Evaluation of round 2 dice roll
            
            if(dice3 + dice4 == 7)
            {
                cout << "You lose!" << endl;
                chips -= bet;
            }
            else
            {
                //Continues rolling dice if parameters not met
                do
                {
                    dice3 = rand()%6+1;
                    dice4 = rand()%6+1;
                    int counter = 3;
                    cout << "ROUND " << counter << ":" << endl
                            << "Dice 1: " << dice3 << endl
                            << "Dice 2: " << dice4 << endl
                            << "Sum: " << dice3 + dice4 << endl << endl;
                    counter++;
                    if(dice3 + dice4 == 7)
                    {
                        cout << "You lose." << endl;
                        chips -= bet;
                    }
                }
                while((dice3 + dice4 != dice1 + dice2) || dice3 + dice4 != 7);
                if(dice3 + dice4 == dice1 + dice2)
                {
                    cout << "You win!" << endl;
                    chips += bet;                    
                }
            }
        } 
        cout << "Total chips remaining: " << chips << endl << endl;
        cout << "Play again?" << endl
                << "    1. Yes" << endl
                << "    2. No" << endl << endl
                << "Enter your choice: ";
        cin >> response;
        cout << endl << endl;
    }
    while(chips > 0 && response == 1);
}

/*
 * 
 */
int main(int argc, char** argv) {

    int chips = 100, response;
    do
    {    
        cout << "Play a game? " << endl
                << "    1. Yes" << endl
                << "    2. No" << endl << endl
                << "Enter your choice: ";
        cin >> response;
        cout << endl;
        if(response == 1)
        {
            cout << "Choose a game:" << endl;
            cout << "   1. Roulette" << endl
                    << "   2. Craps" << endl << endl
                    << "Enter your choice: ";
            int choice;
            cin >> choice;
            cout << endl << endl;
            switch(choice)
            {
                case 1:
                    Roulette(chips);
                    break;
                case 2:
                    Craps(chips);
                    break;
                default:
                    cout << "Invalid Input!";
            }
            cout << "You have " << chips << " chips remaining." << endl
                << "Do you wish to play another game?" << endl
                    << "    1. Yes" << endl
                    << "    2. No" << endl << endl
                    << "Enter your choice: "; 
            cin >> response;
            cout << endl << endl;
        }
    }
    while(response == 1);
    
    return 0;
}
